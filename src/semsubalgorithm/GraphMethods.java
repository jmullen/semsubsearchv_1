/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author joemullen
 */
public class GraphMethods {

    String startingConceptClass;

    // returns four counts for each node in the subgraph;
    // [totaledges][incomingedges][outgoing edges][symmetricaledges]
    // in the form of a 2D int array
    public int[][] getGraphEdgeset(DirectedGraph<String, DefaultEdge> subgraph2) {

        DirectedGraph<String, DefaultEdge> localSubgraph = subgraph2;
        Set<String> conSmall = localSubgraph.vertexSet();

        Iterator<String> conits = conSmall.iterator();
        int count1 = 0;
        int[][] edgeset1 = new int[(int) conSmall.size()][4];

        while (conits.hasNext()) {

            String node1 = conits.next();
            Set<DefaultEdge> totaledges2 = localSubgraph.edgesOf(node1);
            Set<DefaultEdge> incoming2 = localSubgraph.incomingEdgesOf(node1);
            Set<DefaultEdge> outgoing2 = localSubgraph.outgoingEdgesOf(node1);
            edgeset1[count1][0] = (int) totaledges2.size();
            edgeset1[count1][1] = (int) incoming2.size();
            edgeset1[count1][2] = (int) outgoing2.size();

            ArrayList<String> incomingEdgesNodes = new ArrayList<String>();
            for (DefaultEdge edge1 : incoming2) {
                //
                String tar = subgraph2.getEdgeSource(edge1);
                incomingEdgesNodes.add(tar);

            }

            ArrayList<String> outgoingEdgesNodes = new ArrayList<String>();
            for (DefaultEdge edge1 : outgoing2) {
                //
                String tar = subgraph2.getEdgeTarget(edge1);
                outgoingEdgesNodes.add(tar);

            }
            int reverse = 0;
            for (int i = 0; i < incomingEdgesNodes.size(); i++) {
                String str1 = (String) incomingEdgesNodes.get(i);
                for (int j = 0; j < outgoingEdgesNodes.size(); j++) {
                    String str2 = (String) outgoingEdgesNodes.get(j);
                    if (str2.equalsIgnoreCase(str1)) {
                        reverse++;
                    }
                }

                edgeset1[count1][3] = reverse;

            }

            count1++;
        }

        // .out.println("edgeset  " + Arrays.deepToString(edgeset1));

        return edgeset1;

    }

    // returns a hashmap with the String (name) and int [] (connectivity) of
    // each node in the network
    // int [0]= total
    // this DOESN'T WORK!!!
    public HashMap<String, int[]> nameEdgeset(
            DirectedGraph<String, DefaultEdge> graph2) {
        DirectedGraph<String, DefaultEdge> localSubgraph = graph2;
        Set<String> conSmall = localSubgraph.vertexSet();

        String vertexSet1 = conSmall.toString();
        // String vertexSet = vertexSet1.substring(1, vertexSet1.length()-1);
        // String [] arrayVertexSet = vertexSet.split(", ");
        HashMap<String, int[]> allInfo = new HashMap<String, int[]>(
                conSmall.size());
        //int[][] edgeset1 = new int[(int) conSmall.size()][5];
        // for (int i= 0; i<arrayVertexSet.length;i++ ){

        // String node1 = arrayVertexSet [i];

        int count1 = 0;

        Iterator<String> conits = conSmall.iterator();
        while (conits.hasNext()) {
            // for (String node1 : conSmall){
            int[] local = new int[4];
            String node1 = conits.next();
            Set<DefaultEdge> totaledges2 = localSubgraph.edgesOf(node1);
            Set<DefaultEdge> incoming2 = localSubgraph.incomingEdgesOf(node1);
            Set<DefaultEdge> outgoing2 = localSubgraph.outgoingEdgesOf(node1);
            local[0] = (int) totaledges2.size();
            local[1] = (int) incoming2.size();
            local[2] = (int) outgoing2.size();

            ArrayList<String> incomingEdgesNodes = new ArrayList<String>();
            for (DefaultEdge edge1 : incoming2) {
                //
                String tar = graph2.getEdgeSource(edge1);
                incomingEdgesNodes.add(tar);

            }

            ArrayList<String> outgoingEdgesNodes = new ArrayList<String>();
            for (DefaultEdge edge1 : outgoing2) {
                //
                String tar = graph2.getEdgeTarget(edge1);
                outgoingEdgesNodes.add(tar);

            }
            int reverse = 0;

            for (int t = 0; t < incomingEdgesNodes.size(); t++) {
                String str1 = (String) incomingEdgesNodes.get(t);
                for (int j = 0; j < outgoingEdgesNodes.size(); j++) {
                    String str2 = (String) outgoingEdgesNodes.get(j);
                    if (str2.equalsIgnoreCase(str1)) {
                        reverse++;
                    }
                }

            }

            local[3] = reverse;

            allInfo.put(node1, local);

            count1++;
        }

        return allInfo;

    }

    //get the most connected node of a particualr semantic class, or if startclass
    //equals none then just retriece the most highly connected
    public String mostConnectedSem(HashMap<String, int[]> edgeset, String conceptClass, Map<String, String> queryNODEInfo) {
        String mostConnected = "";
        int highest = 0;
        Iterator<String> it = edgeset.keySet().iterator();
        while (it.hasNext()) {
            String name = (String) it.next();
            int[] local = edgeset.get(name);
            if (local[0] > highest) {
                if (conceptClass.equals("any")) {
                    highest = local[0];
                    mostConnected = name;
                } else {
                    if (queryNODEInfo.get(name).equals(conceptClass)) {
                        highest = local[0];
                        mostConnected = name;
                    }
                }
            }
        }
        //assign the correct conceptClass as the startingConceptClass
        if (conceptClass.equals("any")) {
            startingConceptClass = queryNODEInfo.get(mostConnected);
        } else {
            startingConceptClass = conceptClass;
        }
        
       // System.out.println("SCCinmeth:"+ startingConceptClass);
       // System.out.println("MCSinmeth:"+mostConnected);
        return mostConnected;
    }

    // returns the String (name) of the most highly connected node in a network
    // takes the hashmap as an argument
    public String mostConnectedTop(HashMap<String, int[]> edgeset) {

        String mostConnected = "";
        int highest = 0;

        Iterator<String> it = edgeset.keySet().iterator();

        while (it.hasNext()) {
            String name = (String) it.next();
            int[] local = edgeset.get(name);

            if (local[0] > highest) {
                highest = local[0];
                mostConnected = name;
                // .out.println("Most Connected Node in sub " + name);
            }
        }


        return mostConnected;

    }

    // returns all matches from target graph of the most highly connected node
    // in sub
    public ArrayList<String> startingNodesSem(String highestfromsub,
            HashMap<String, int[]> subnameedgset,
            HashMap<String, int[]> tarnameedgset, String ConceptClass) {
        int[] local = subnameedgset.get(highestfromsub);
        int connetivity = local[0];
        ArrayList<String> matches = new ArrayList<String>();
        Iterator<String> it = tarnameedgset.keySet().iterator();

        while (it.hasNext()) {
            String name = (String) it.next();
            int[] local2 = tarnameedgset.get(name);

            if (local2[0] >= connetivity) {

                matches.add(highestfromsub + ",,," + name);
                // .out.println(name);
            }

        }
        //System.out.println("connectivity matches in large graph "
        //	+ matches.toString());
        return matches;

    }

    // returns all matches from target graph of the most highly connected node
    // in sub
    public ArrayList<String> startingNodes(String highestfromsub,
            HashMap<String, int[]> subnameedgset,
            HashMap<String, int[]> tarnameedgset) {
        int[] local = subnameedgset.get(highestfromsub);
        int connetivity = local[0];
        ArrayList<String> matches = new ArrayList<String>();
        Iterator<String> it = tarnameedgset.keySet().iterator();

        while (it.hasNext()) {
            String name = (String) it.next();
            int[] local2 = tarnameedgset.get(name);

            if (local2[0] >= connetivity) {

                matches.add(highestfromsub + ",,," + name);
                // .out.println(name);
            }

        }
        //System.out.println("connectivity matches in large graph "
        //	+ matches.toString());
        return matches;

    }

    // returns the size (nodes) of the subgraph
    public int getGraphSize(DirectedGraph<String, DefaultEdge> entry) {
        int thidid = entry.vertexSet().size();
        // System.out.println("size of graph  " + thidid);
        return thidid;

    }

    // returns all the nodes from a graph in the form of a ArrayList (in no
    // particular order)
    public ArrayList<String> getInitialNodes(
            DirectedGraph<String, DefaultEdge> network) {
        String[] conceptsbiggraph = toStringArray(network.vertexSet());
        ArrayList<String> nodesBig = new ArrayList<String>(
                conceptsbiggraph.length);
        for (String s : conceptsbiggraph) {
            nodesBig.add(s);
            // .out.println(s);
        }

        return nodesBig;

    }

    // allows for the conversion of a Set of Strings to an array of Strings
    public String[] toStringArray(Set<String> conLarge1) {
        Set<String> localSet = conLarge1;
        String[] conceptSet = new String[conLarge1.size()];
        Iterator<String> conc = localSet.iterator();
        int count1 = 0;
        while (conc.hasNext()) {
            String thisT = "";
            thisT = conc.next();
            conceptSet[count1] = thisT;
            count1++;

        }
        return conceptSet;
    }

    // prints out all the outgoing nodes of a state- NOT including those that
    // are already in the 'match'
    // gives the new nodes back in the form of a string array
    public String[] tOut(DirectedGraph<String, DefaultEdge> graph,
            String[] originalNodes) {
        //System.out.println("originalNodes: "
        //	+ Arrays.deepToString(originalNodes));

        ArrayList<String> newnodesOutLIST = new ArrayList<String>();

        for (int i = 0; i < originalNodes.length; i++) {
            String localNode = originalNodes[i].trim();
            if (!localNode.equals("NULL_NODE")) {

                Set<DefaultEdge> edges = graph.outgoingEdgesOf(localNode);
                for (DefaultEdge edgeysys : edges) {
                    String tar = graph.getEdgeTarget(edgeysys);
                    int matches = 0;

                    int count = 0;
                    //testing it isnt one of the original nodes
                    while (count < originalNodes.length) {

                        String heree = originalNodes[count].trim();
                        //System.out.println("comparing heree: "+ heree+"  too: "+ tar);
                        if (heree.equals(tar.trim())) {
                            matches++;
                        }
                        count++;
                    }
                    //testing it has not already been added to newNodesOutlist

                    for (String node : newnodesOutLIST) {
                        if (node.equals(tar)) {
                            matches++;
                        }

                    }


                    if (matches == 0) {
                        //System.out.println("we are adding this node: " + tar);
                        newnodesOutLIST.add(tar);

                    }

                }

            }
        }

        String[] outNodes = new String[newnodesOutLIST.size()];
        for (int y = 0; y < newnodesOutLIST.size(); y++) {
            outNodes[y] = newnodesOutLIST.get(y);
        }

        return outNodes;
    }

    // same method as above but takes a single node (for the first s of the
    // search)
    public String[] tOut(DirectedGraph<String, DefaultEdge> graph,
            String originalNode) {

        String[] single = new String[1];
        single[0] = originalNode;

        return tOut(graph, single);

    }

    // prints out all the Ingoing nodes of a state- NOT including those that are
    // already in the 'match'
    // gives the new nodes back in the form of a string array
    // CAN WE MERGE THE TWO METHODS?!? THEY ARE BOTH USED!!!
    public String[] tIn(DirectedGraph<String, DefaultEdge> graph,
            String[] originalNodes) {

        ArrayList<String> newnodesInLIST = new ArrayList<String>();

        for (int i = 0; i < originalNodes.length; i++) {
            String localNode = originalNodes[i].trim();
            if (!localNode.equals("NULL_NODE")) {

                Set<DefaultEdge> edges = graph.incomingEdgesOf(localNode);

                for (DefaultEdge edgeysys : edges) {
                    String tar = graph.getEdgeSource(edgeysys);
                    int matches = 0;

                    int count = 0;

                    while (count < originalNodes.length) {

                        String heree = originalNodes[count];
                        if (heree.equals(tar)) {
                            matches++;
                        }
                        count++;
                    }

                    for (String node : newnodesInLIST) {
                        if (node.equals(tar)) {
                            matches++;
                        }

                    }

                    if (matches == 0) {
                        newnodesInLIST.add(tar);

                    }

                }

            }
        }

        String[] inNodes = new String[newnodesInLIST.size()];
        for (int y = 0; y < newnodesInLIST.size(); y++) {
            inNodes[y] = newnodesInLIST.get(y);
        }

        return inNodes;
    }

    // same method as above but takes a single node (for the first step of the
    // search)
    public String[] tIn(DirectedGraph<String, DefaultEdge> graph,
            String originalNode) {

        String[] single = new String[1];
        single[0] = originalNode;

        return tIn(graph, single);

    }

    public String getStartingCC() {
        return startingConceptClass;
    }
}
