/*
 * Class takes a sub graph
 * If it has |V|  > 4 then the graph is split into graphs containing nodes of !> 4
 */
package semsubalgorithm.graphSplitting;
/*
 * How can we quantify splitting the subgraphs?
 * Based on connectivity?!?!
 * Graph measure 
 */

import semsubalgorithm.graphSplitting.SplitInstance;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import semsubalgorithm.GraphMethods;
import semsubalgorithm.QueryGraph;

/**
 *
 * @author joemullen
 */
//make this recursive- why can't we just keep calling slit until all subs
//are of size 4 max??--- need the results of the basic algorithm to defend
//why we use 4 node subs and not just three.
public class SplittingLargeSubs extends GraphMethods {

    QueryGraph q;
    DirectedGraph<String, DefaultEdge> original;
    UndirectedGraph<String, DefaultEdge> UDorigi;
    Map<String, String> originalSemInfo;
    Set<SplitInstance> newSubs;
    HashMap<String, ArrayList<String>> nodeForeachGraph;
    Set<DefaultEdge> extraEdges;
    HashMap<String, int[]> edgset;
    String mostConnected;
    int subsizes = 3;
    SplitInstance splitGraph;
    QueryGraph orig;
    boolean largestFirst;

    public static void main(String[] args) {
        //QueryGraph q = new QueryGraph(SemanticSub.EIGHTNODE);

        //SplittingLargeSubs split = new SplittingLargeSubs(q);




    }

    public SplittingLargeSubs(QueryGraph s, boolean largestFirst) {
        this.largestFirst = largestFirst;
        this.orig =s;
        this.q = s;
        this.original = s.getSub();
        this.originalSemInfo = s.getQueryNodeInfo();
        this.newSubs = new HashSet<SplitInstance>();
        this.extraEdges = new HashSet<DefaultEdge>();
        this.edgset = nameEdgeset(original);
        this.mostConnected = mostConnectedTop(edgset);
        this.UDorigi = new SimpleGraph<String, DefaultEdge>(DefaultEdge.class);
        this.nodeForeachGraph = new HashMap<String, ArrayList<String>>();
        this.splitGraph = new SplitInstance();
        long startTime = System.currentTimeMillis();
        run();
        long endTime = System.currentTimeMillis();
        System.out.println("[INFO] Splitting large subgraph took " + (endTime - startTime) / 1000 + " seconds");
        System.out.println("[INFO] ----------------------------------------------------------");



    }

    public void convertToUndirected() {
        Set<String> allNodes = original.vertexSet();
        for (String no : allNodes) {
            UDorigi.addVertex(no);
        }
        Set<DefaultEdge> allEdges = original.edgeSet();
        for (DefaultEdge ed : allEdges) {
            String edgey = ed.toString().substring(1, ed.toString().length() - 1);
            String[] split = edgey.split(":");
            String from = split[0].trim();
            String to = split[1].trim();
            UDorigi.addEdge(from, to);
            // System.out.println(ed.toString().substring(1, ed.toString().length() - 1));

        }

        //System.out.println("UD: " + UDorigi.toString());

    }

    public void run() {

        System.out.println("[INFO] Starting to split sub of size " + original.vertexSet().size());
        //getNumberOfnewSubs(original.vertexSet().size());
        //getSizeOfNewSubs(getNumberOfnewSubs(original.vertexSet().size()), original.vertexSet().size());
        convertToUndirected();
        shortestPath();
        getGraphEdgeset(original);
        createSubs();


    }

    public SplitInstance getSplit() {

        return splitGraph;
    }

    @SuppressWarnings("unchecked")
    public void shortestPath() {
        Set<String> allNodes = UDorigi.vertexSet();
        //System.out.println(allNodes.toString());
        String pair = "";
        int distance = 0;
        for (String no : allNodes) {
            String start = no;
            for (String no2 : allNodes) {

                DijkstraShortestPath tr2 = new DijkstraShortestPath(UDorigi, start, no2);
                int dis = tr2.getPathEdgeList().size();
                if (dis > distance) {
                    distance = dis;
                    pair = start + ">>" + no2;
                }

            }

        }

        String[] split2 = pair.split(">>");

        DijkstraShortestPath tr2 = new DijkstraShortestPath(UDorigi, mostConnected, split2[0]);
        int disOne = tr2.getPathEdgeList().size();
        String name1 = "one";
        ArrayList<String> nodes1 = nodesFromEdgeList(tr2.getPathEdgeList());
        nodeForeachGraph.put(name1, nodes1);


        DijkstraShortestPath tr3 = new DijkstraShortestPath(UDorigi, mostConnected, split2[1]);
        int distwo = tr3.getPathEdgeList().size();
        String name2 = "two";
        ArrayList<String> nodes2 = nodesFromEdgeList(tr3.getPathEdgeList());
        nodeForeachGraph.put(name2, nodes2);

        ArrayList<String> leftowa = nodesNotIncluded(nodesFromEdgeList(tr2.getPathEdgeList()), nodesFromEdgeList(tr3.getPathEdgeList()));
        allocatingLefOverNodes(leftowa);




    }

    public void allocatingLefOverNodes(ArrayList<String> notMatched) {

        //need to improve the rules
        for (String nod : notMatched) {
            // System.out.println("Allocating: " + nod);
            //can only be added to one subgraph anyway!!
            if (UDorigi.degreeOf(nod) == 1) {
                Set<DefaultEdge> edgis = UDorigi.edgesOf(nod);
                String targ = "";
                for (DefaultEdge s : edgis) {
                    //System.out.println(nod + "  " + UDorigi.getEdgeTarget(s));
                    String src = UDorigi.getEdgeSource(s);
                    String tar = UDorigi.getEdgeTarget(s);
                    if (!nod.equals(src)) {
                        targ = src;
                    } else {
                        targ = tar;
                    }
                }

                Set<String> graphs = nodeForeachGraph.keySet();
                String addtoo = "";
                for (String gr : graphs) {
                    ArrayList<String> thisgraph = nodeForeachGraph.get(gr);
                    if (thisgraph.contains(targ)) {
                        addtoo = gr;
                    }

                }

                ArrayList<String> h = nodeForeachGraph.get(addtoo);
                h.add(nod);
                nodeForeachGraph.put(addtoo, h);

            } else {
                //what do we do with the nodes that are left over and are connected to both subgraphs??
                Set<DefaultEdge> ed = original.edgesOf(nod);
                //if there is a node that it has a symmetirc relation with then we add it to that graph

                //add it to the smallest graph if possible
                Set<String> graphs = nodeForeachGraph.keySet();
                int size = 100;
                String addtoo = "";
                for (String gr : graphs) {
                    ArrayList<String> thisgraph = nodeForeachGraph.get(gr);
                    if (thisgraph.size() < size) {
                        size = thisgraph.size();
                        addtoo = gr;
                    }

                }

                ArrayList<String> h = nodeForeachGraph.get(addtoo);
                h.add(nod);
                nodeForeachGraph.put(addtoo, h);


            }

        }

        // System.out.println("[INFO] New Subgraphs: " + nodeForeachGraph.toString());
    }

    public ArrayList<String> nodesNotIncluded(ArrayList<String> one, ArrayList<String> two) {
        ArrayList<String> local = new ArrayList<String>(one);
        local.addAll(two);

        ArrayList<String> noMatch = new ArrayList<String>();
        Set<String> all = original.vertexSet();
        for (String node : all) {
            if (!local.contains(node)) {
                noMatch.add(node);
            }
        }


        return noMatch;

    }

    public ArrayList<String> nodesFromEdgeList(List<DefaultEdge> edgelist) {
        ArrayList<String> nodes = new ArrayList<String>();

        int count = 0;
        for (DefaultEdge rel : edgelist) {
            String source = UDorigi.getEdgeSource(rel);
            String targ = UDorigi.getEdgeTarget(rel);
            count++;

            if (count == 1) {

                nodes.add(source);
                nodes.add(targ);

            } else {
                if (!nodes.contains(targ)) {
                    nodes.add(targ);
                } else {
                    nodes.add(source);
                }
            }

        }

        return nodes;
    }

    public void createSubs() {
        Set<String> graphs = nodeForeachGraph.keySet();
        String addtoo = "";
        int count = 0;
        Set<DefaultEdge> allEdgeFromOrig = original.edgeSet();
        Set<DefaultEdge> added = new HashSet<DefaultEdge>();
        Set<DefaultEdge> NotADDED = new HashSet<DefaultEdge>();
        QueryGraph q1 = null;
        QueryGraph q2 = null;

        for (String gr : graphs) {
            count++;
            ArrayList<String> thisgraph = nodeForeachGraph.get(gr);
            DirectedGraph<String, DefaultEdge> name = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
            for (String node : thisgraph) {
                name.addVertex(node);
            }

            for (DefaultEdge loc : allEdgeFromOrig) {
                String from = original.getEdgeSource(loc);
                String to = original.getEdgeTarget(loc);
                if (name.containsVertex(from) && name.containsVertex(to)) {
                    name.addEdge(from, to);
                    added.add(loc);
                }
            }

            //need a method here that extracts the correct semantic info 


            //need to create the semantics here as well--- if we want to use them!!
            //newSubs.add();
            if (count == 1) {
                //WE ARE HERE- NEED TO ADD THE SEMANTICS TO THE SPLIT
                //q1 = new QueryGraph(name, gr, getSemforSplit(name));

            }

            if (count == 2) {
                //q2 = new QueryGraph(name, gr, getSemforSplit(name));

            }
            System.out.println("[INFO] Created the sub: " + name.toString());
            //System.out.println("[INFO] Created the sub: " + name.toString());

        }

        for (DefaultEdge ed : allEdgeFromOrig) {
            if (!added.contains(ed)) {
                NotADDED.add(ed);
            }

        }

        //System.out.println("alledges from orig: " + allEdgeFromOrig.toString());
        //System.out.println("alledges added : " + added.toString());
        System.out.println("[INFO] Edges left over: " + NotADDED.toString());
        System.out.println("[INFO] Crossover node " + getCrossoverNode() + " with edgeset " + Arrays.toString(getCrossoverNodeEdgeset()));

       splitGraph = new SplitInstance(q1, q2, orig, mostConnected, largestFirst);

    }

    //calculates how many new subgraphs will (ideally) be created
    public int getNumberOfnewSubs(int graphSize) {

        int g = graphSize + 1;
        int newSubs = 1;
        for (int i = 0; i < 10; i++) {

            if (g < subsizes && g > 0) {
                newSubs = i + 1;
                break;
            }
            if (g == 0) {
                newSubs = i;
                break;
            } else {
                g = g - subsizes;
            }

        }

        //System.out.println("There will be this many new graphs " + newSubs);
        return newSubs;
    }

    public ArrayList<Integer> getSizeOfNewSubs(int numberofsubs, int graphsize) {
        ArrayList<Integer> insubsizes = new ArrayList<Integer>();
        int withOverlap = graphsize + (numberofsubs - 1);
        for (int i = 0; i < 10; i++) {

            if (withOverlap > subsizes) {
                insubsizes.add(subsizes);
            } else if (withOverlap < subsizes || withOverlap == subsizes) {
                insubsizes.add(withOverlap);
                break;

            }
            withOverlap = withOverlap - subsizes;
        }
        //System.out.println("Of these sizes " + insubsizes.toString());

        return insubsizes;

    }

    public Map<String, String> getSemforSplit(DirectedGraph<String, DefaultEdge> sub) {
        Map<String, String> ccs = new HashMap<String, String>();
        //get the vertex set of the new graph
        Set<String> vertex = sub.vertexSet();
        for (String node : vertex) {

            if (originalSemInfo.containsKey(node)) {

                ccs.put(node, originalSemInfo.get(node));

            }


        }


        return ccs;


    }

    public String getCrossoverNode() {
        return mostConnected;
    }
    //get the edgeset of the most connected node as it is in the original subgraph

    public int[] getCrossoverNodeEdgeset() {
        return edgset.get(mostConnected);
    }

    public Set<SplitInstance> getNewSubs() {
        return newSubs;
    }

    public Set<DefaultEdge> getExtraEdges() {

        return extraEdges;

    }

    public QueryGraph getOirignalQueryGraph() {
        return q;

    }
}
