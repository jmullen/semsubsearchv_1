/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.graphSplitting;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import semsubalgorithm.QueryGraph;
import semsubalgorithm.RandomGraph;
import semsubalgorithm.RandomQuery;
import semsubalgorithm.SerialisedGraph;
import semsubalgorithm.SourceGraph;

/**
 *
 * @author joemullen
 */
public class SplitInstance {

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("myobject.data");
        
        
        RandomQuery thr = new RandomQuery(source, 6, "Compound");
        QueryGraph q = thr.getRanQuery();
        
        
        RandomGraph sem = new RandomGraph(source, 100000, 1);
        sem.createSemanticGraph();
        SourceGraph random = sem.getRandomSourceGraph();
        
        
        
        
        System.out.println("--------------------------Smallest First");
        SplittingLargeSubs split1 = new SplittingLargeSubs(q, true);
        SplitInstance s1 = split1.getSplit();
        System.out.println(s1.getOrig().toString());
        
        System.out.println("really??");
        SplitSearch spl1 = new SplitSearch(s1, random, 0.8);
        spl1.run();
        System.out.println(spl1.getSearchTime() + "|" + spl1.getMappingTime());
        //spl1 = null;
        
        System.out.println("--------------------------Biggest First");
        SplittingLargeSubs split2 = new SplittingLargeSubs(q, false);
        SplitInstance s2 = split2.getSplit();
        SplitSearch spl2 = new SplitSearch(s2, random, 0.8);
        spl2.run();
        System.out.println(spl2.getSearchTime() + "|" + spl2.getMappingTime());
        spl2 = null;

    }
    QueryGraph q1;
    QueryGraph q2;
    QueryGraph orig;
    String crossover;
    boolean check = false;
    Set<QueryGraph> matches;

    public SplitInstance() {
    }

    public SplitInstance(QueryGraph q1, QueryGraph q2, QueryGraph orig, String crossoverNode, boolean biggestFirst) {

        if (biggestFirst == true) {
            if (q1.getSub().vertexSet().size() > q2.getSub().vertexSet().size()) {
                this.q1 = q2;
                this.q2 = q1;

            } else {
                this.q1 = q1;
                this.q2 = q2;

            }
        } else {
            this.q1 = q1;
            this.q2 = q2;



        }
        this.crossover = crossoverNode;
        this.orig = orig;

        System.out.println(
                "created new splitinstance");

    }

    public QueryGraph getQueryGraph1() {
        return q1;

    }

    public QueryGraph getQueryGraph2() {
        return q2;

    }

    public QueryGraph getOrig() {
        return orig;

    }

    public String getCrossoverNode() {

        return crossover;
    }

    public boolean checkSplitSizes() {
        boolean check = true;


        if (q1.getSub().vertexSet().size() > 4) {
            check = false;
        }

        if (q2.getSub().vertexSet().size() > 4) {
            check = false;
        }

        return check;

    }

    public boolean getCheck() {

        return check;

    }

    public void setCheck(boolean c) {

        this.check = c;

    }

    @Override
    public String toString() {

        String summary = "q1: " + q1.getSub().toString() + "  g2: " + q2.getSub().toString() + "  crossoverNode: " + crossover;

        return summary;


    }

    public Set<QueryGraph> getMatches() {

        return matches;

    }

    public void SetMatches(Set<QueryGraph> s) {

        this.matches = s;


    }
}
