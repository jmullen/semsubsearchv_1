/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.semanticDistanceSubgraphExclusion;

/**
 *
 * @author joemullen
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import semsubalgorithm.semanticDistanceSubgraphExclusion.SemanticDistanceCalculator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sjcockell
 */
public class SemanticDistanceCalculatorTest {

    /**
     * 
     * @param args 
     */
    
    public static void main(String [] args) {
        SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();
        
        try {    
            //System.out.println("are we getting to here?");
            System.out.println(""+sdc.getEdgeDistance("has_parent", "has_child"));
            System.out.println(""+sdc.getEdgeDistance("has_child","has_parent"));
            System.out.println(""+sdc.getEdgeDistance("has_child","has_child"));
            System.out.println(""+sdc.getEdgeDistance("has_parent","has_parent"));
            System.out.println(""+sdc.getNodeDistance("Protein", "Compound"));
            System.out.println(""+sdc.getNodeDistance("Target", "Indications"));
            System.out.println("All ccs :"+ sdc.getAllConceptClasses().toString());
            System.out.println("All rts :"+ sdc.getAllRelationTypes().toString());
            
        } catch (Exception ex) {
            Logger.getLogger(SemanticDistanceCalculatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


