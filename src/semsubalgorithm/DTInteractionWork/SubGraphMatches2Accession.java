/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.DTInteractionWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import semsubalgorithm.QueryGraph;

/**
 *
 * @author joemullen
 */

public class SubGraphMatches2Accession {
    
    private Map<String, String> DRUG;
    private Map<String, String> PROTEIN;
    private ParseDrugBankData pi;
    private String read;
    private String write;
    private QueryGraph q;

    public static void main(String[] args) throws FileNotFoundException, IOException {
        
    }
    

    public SubGraphMatches2Accession(String read, QueryGraph q) throws FileNotFoundException, IOException {
        this.q = q;
        this.read = read;
        this.pi =  new ParseDrugBankData();
        pi.getDatassetInfo();
        pi.getDrugBankV3Info();
        this.DRUG = pi.getDrugBank2NAMEsDATASET();
        this.PROTEIN = pi.getUniprot2NAMEsDATASET();

    }

    public void mapp() throws FileNotFoundException, IOException {
        
        int elements = 2;
        int drugpostion = 0;
        int proteinposition = 1;

        BufferedReader br = new BufferedReader(new FileReader(read));
        BufferedWriter bw = new BufferedWriter(new FileWriter("Results/DB3WORK/UniquePairs/OPUNIQUPAIRS"+ q.getMotifName()+".txt"));

        Set<String> pairs = new HashSet<String>();
        int count = 0;
        int replication = 0;
        int nullcount = 0;
        String line;
        while ((line = br.readLine()) != null) {


            if (line.startsWith("IN:")) {


                String local = line.substring(3);
                String[] split = local.split("\t");
                if (split.length < 2) {

                   // System.out.println(local + " less than 2 elements");
                    nullcount++;
                } else {

                    String MATCH = DRUG.get(split[drugpostion].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + PROTEIN.get(split[proteinposition].replaceAll("[^A-Za-z0-9]", "").toLowerCase().trim()) + "\t" + split[drugpostion] + "\t" + split[proteinposition];
                    if (MATCH.contains("null")) {
                        nullcount++;

                       // System.out.println(MATCH);
                    } else {
                        if (pairs.contains(MATCH)) {

                            replication++;
                        } else {
                            pairs.add(MATCH);
                            count++;
                        }
                    }
                }
            }
        }

        System.out.println("SUMMARY--------------");
        System.out.println((count + replication + nullcount) + " compound -> target pairs were parsed");
        System.out.println(nullcount + " contained null values");
        System.out.println(replication + " of these were repeats");
        System.out.println(count + " were added to file as unique " + pairs.size());


        for (String mat : pairs) {
            bw.append(mat + "\n");
        }
        bw.close();

    }

  
}

    

