/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.DTInteractionWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author joemullen
 */
public class Comparing2ListsOfRelations {

    ArrayList<String> pairs1 = new ArrayList<String>();
    ArrayList<String> pairs2 = new ArrayList<String>();
    ArrayList<String> nonmatchpairs1 = new ArrayList<String>();
    ArrayList<String> nonmatchpairs2 = new ArrayList<String>();
    ArrayList<String> allMATCHES = new ArrayList<String>();
    Map<String, String> DB2DBACCS2NAME = new HashMap<String, String>();
    Map<String, String> DB2UNIACCS2NAMES = new HashMap<String, String>();
    Map<String, String> DB3DBACCSNAME = new HashMap<String, String>();
    Map<String, String> DB3UNIACCS2NAMES = new HashMap<String, String>();
    String file1;
    String file2;
    String idfile1;
    String idfile2;
    int unavailable = 0;
    int repeatsDB2 = 0;
    int singleDB2 = 0;
    int singleDB3 = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        Comparing2ListsOfRelations ma = new Comparing2ListsOfRelations("/Users/joemullen/Desktop/Comparing2/DB3 Unique/all_Valid.txt", "db3", "/Users/joemullen/Desktop/Results For Paper/SemSearch Results/CIT/OPUNIQUPAIRS.txt", "CIT");
        ma.getfile1rels();
        ma.getfile2rels();
        ma.match();

    }
    
    public Comparing2ListsOfRelations(String file1, String file1id, String file2, String file2id){
        
        this.file1 = file1;
        this.file2 = file2;
        this.idfile1 = file1id;
        this.idfile2 = file2id;
   
    
    }

    public void getfile1rels() throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file1));

        String line;
        while ((line = br.readLine()) != null) {

            String[] split = line.split("\t");

            if (split.length > 1) {
                String from = split[0];
                String to = split[1];

//                String fromName = split[2];
 //               String toName = split[3];
 //               fromName = fromName.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
 //               toName = toName.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
                //from = from.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
                //to = to.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();


                if (pairs1.contains((from + "," + to))) {
                    repeatsDB2++;
                } else {
                    pairs1.add(from + "\t" + to);
                    //          DB2DBACCS2NAME.put(fromName, from);
                    //             DB2UNIACCS2NAMES.put(toName, to);
                }
            } else {
                //System.out.println("NOT 2 ELEMENTS IN STRING: " + line);
                singleDB2++;

            }
        }



    }

    public void getfile2rels() throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file2));
        String line;
        while ((line = br.readLine()) != null) {

            String[] split = line.split("\t");

            if (split.length > 1) {
                String from = split[0];
                String to = split[1];
                
                if(to.equals("Not Available")){
                    unavailable ++;
                
                }

//                String fromName = split[2];
                //              String toName = split[3];
                //from = from.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
                //to = to.replaceAll("[^A-Za-z0-9]", "").trim().toLowerCase();
                //            DB3DBACCSNAME.put( fromName, from);
                //          DB3UNIACCS2NAMES.put( toName, to);
                pairs2.add(from + "\t" + to);
            } else {
                // System.out.println("NOT 2 ELEMENTS IN STRING: " + line);
                singleDB3++;

            }
        }



    }

    public void match() throws IOException {
        int matches = 0;
        int nomatchesDB2 = 0;
        int nomatchesDB3 = 0;
        for (String pair : pairs2) {
            if (pairs1.contains(pair)) {
                System.out.println("MATCH: " + pair);
                allMATCHES.add(pair);


            } else {
                //System.out.println("NO MATCH");
                //nomatchesDB3 ++;
                nonmatchpairs2.add(pair);
            }

        }

        for (String pair : pairs1) {
            if (pairs2.contains(pair)) {
                //System.out.println("MATCH");
                if (!allMATCHES.contains(pair)) {
                    allMATCHES.add(pair);
                }


            } else {
                //System.out.println("NO MATCH");
                //nomatchesDB2 ++;
                nonmatchpairs1.add(pair);
            }

        }

        System.out.println("--------------------------------SUMMARY");
        System.out.println(allMATCHES.size() + "  MATCHES");
        System.out.println("--------------------------------FILE 1");
        System.out.println(pairs1.size() + "  pairs in file");
        System.out.println(repeatsDB2 + " repeated in file");
        System.out.println(singleDB2 + "  not recognised");
        System.out.println(nonmatchpairs1.size() + "  none matches");
        System.out.println(unavailable + " includes a target that is unavailable");
        System.out.println("--------------------------------FILE 2");
        System.out.println(pairs2.size() + "  pairs in the file");
        System.out.println(singleDB3 + "  not recognised");
        System.out.println(nonmatchpairs2.size() + "  none matches");

//
//        BufferedWriter bw2 = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Comparing2/nomatches"+ idfile1+ ".txt"));
//        for (String pair : nonmatchpairs1) {
//            bw2.append(pair + "\n");
//        }
//
//        bw2.close();
//
//        BufferedWriter bw = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Comparing2/nomatches"+ idfile2+ ".txt"));
//        for (String pair : nonmatchpairs2) {
//            bw.append(pair + "\n");
//        }
//        bw.close();


        BufferedWriter bw3 = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Comparing2/matches" + idfile1 + "&"+ idfile2+".txt"));
        for (String pair : allMATCHES) {
            bw3.append(pair + "\n");
        }
        bw3.close();

//
//        BufferedWriter bw4 = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Comparing2drugBanks/db3db2names.txt"));
//        Set<String> l = DB3DBACCSNAME.keySet();
//        for (String pair : l) {
//            bw4.append(l + "\t" + DB3DBACCSNAME.get(l) + "\n");
//        }
//        bw4.close();
//
//        BufferedWriter bw5 = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Comparing2drugBanks/db3uni2names.txt"));
//        Set<String> lr = DB3UNIACCS2NAMES.keySet();
//        for (String pair : lr) {
//            bw5.append(lr + "\t" + DB3UNIACCS2NAMES.get(lr) + "\n");
//        }
//        bw5.close();
//
//        BufferedWriter bw6 = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Comparing2drugBanks/db2db2names.txt"));
//        Set<String> lrr = DB2DBACCS2NAME.keySet();
//        for (String pair : lrr) {
//            bw6.append(lrr + "\t" + DB2DBACCS2NAME.get(lrr) + "\n");
//        }
//        bw6.close();
//
//        BufferedWriter bw7 = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/Comparing2drugBanks/db2uni2names.txt"));
//        Set<String> lrrr = DB2UNIACCS2NAMES.keySet();
//        for (String pair : lrrr) {
//            bw7.append(lrrr + "\t" + DB2UNIACCS2NAMES.get(bw7));
//        }
//        bw7.close();
    }
    

    public Map<String, String> getDB2DBNAMES() {
        return DB2DBACCS2NAME;
    }

    public Map<String, String> getDB2UNINAMES() {
        return DB2UNIACCS2NAMES;
    }

    public Map<String, String> getDB3DBNAMES() {
        return DB3DBACCSNAME;
    }

    public Map<String, String> getDB3UNINAMES() {
        return DB3UNIACCS2NAMES;
    }
}
