/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.DTInteractionWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Random;

/**
 *
 * @author joemullen
 */
//this class takes a list of DBIDs and a list of UNIPROTIds, and randomly creates N drug interactions
public class RandomDTInteractionTest {

    Set<String> DBIDs;
    Set<String> UNIPROTIDs;
    Set<String> validPAIRS;
    Set<String> randomRELATIONS;
    int numberOfActualMatches;
    ParseDrugBankData pi;

    public static void main(String[] args) throws FileNotFoundException, IOException {


        BufferedWriter bw = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/averages.txt"));
        int numberofrelations = 5000;
        int numberOfTests = 100;

        float[] mean = new float[15];
//        float[] sddown = new float[15];
//        float[] sdup = new float[15];

        for (int d = 0; d < 21; d++) {
            int numberofrelations2 = numberofrelations * d;
            int[] allScores = new int[numberOfTests];
            int total = 0;
            for (int i = 0; i < numberOfTests; i++) {
                RandomDTInteractionTest rt = new RandomDTInteractionTest(numberofrelations2);
                // rt.getInfo();
                rt.createRandomRelations();
                int matches = rt.matches();
                allScores[i] = matches;
                total = total + matches;
            }
            System.out.println("SUMMARY---------- " + numberofrelations2);
           // Arrays.sort(allScores);
            float average = (float) total / numberOfTests;
            for (int y = 0; y < allScores.length; y++) {
                bw.append(allScores[y] + "\t");
            }
            bw.append("\n");

            System.out.println("Mean: " + average + "  " + allScores[0] + "  " + allScores[numberOfTests - 1]);
            // System.out.println(Arrays.toString(allScores));
//            float sd = 0;
//            for (int i = 0; i < allScores.length; i++) {
//                float difffromMean = allScores[i] - average;
//                float squared = difffromMean * difffromMean;
//                sd = sd + squared;
//            }
//
//            float mean2 = sd / numberOfTests;
//            mean[d] = average;
//            float sd2 = (float) Math.sqrt(mean2);
//            float sd3 = sd2 / 2;
//            sddown[d] = mean2 - sd3;
//            sdup[d] = mean2 + sd3;
//            System.out.println("SD: " + sd2);



        }

        System.out.println("MEAN: " + Arrays.toString(mean));
//        System.out.println("SDUP: " + Arrays.toString(sddown));
//        System.out.println("SDDOWN: " + Arrays.toString(sdup));
        bw.close();
    }

    public RandomDTInteractionTest(int number) throws FileNotFoundException, IOException {
        this.pi = new ParseDrugBankData();
        pi.getDatassetInfo();
        pi.getDrugBankV3Info();
        pi.getValidPairsInfo();
        this.DBIDs = pi.getallCompoundsDATASET();
        this.UNIPROTIDs = pi.getallProtiensDATASET();
        this.validPAIRS = new HashSet<String>();
        this.randomRELATIONS = new HashSet<String>();
        this.numberOfActualMatches = number;
        this.validPAIRS = pi.getallValidUNIQUEPairsDB3();
    }

    public void createRandomRelations() {

        //ArrayList<String> randomRelations = new ArrayList<String>();

        String[] drugbank = new String[DBIDs.size()];
        int count = 0;
        for (String id : DBIDs) {
            drugbank[count] = id;
            count++;

        }

        String[] uniprot = new String[UNIPROTIDs.size()];
        int count2 = 0;
        for (String id : UNIPROTIDs) {
            uniprot[count2] = id;
            count2++;
        }

        for (int i = 0; i < numberOfActualMatches; i++) {
            Random random = new Random();
            // randomly selects an index from the arr
            int dbid = random.nextInt(drugbank.length);
            String db = drugbank[dbid];

            int unipro = random.nextInt(uniprot.length);
            String up = uniprot[unipro];
            randomRELATIONS.add(db + "\t" + up);

            // prints out the value at the randomly selected index
            //System.out.println(
            // "Random Relation: " + db + "\t" + up);
        }
        // return randomRelations;

    }

    public int matches() {

        int count = 0;

        for (String rel : randomRELATIONS) {

            if (validPAIRS.contains(rel)) {
                count++;
            }


        }

        //System.out.println("Random relations identified: " + count + "/" + validPAIRS.size());
        // System.out.println("DBIDs: " + DBIDs.size());
        // System.out.println("Uniprots: " + UNIPROTIDs.size());
        // System.out.println("Possible relations: " + UNIPROTIDs.size() * DBIDs.size());
        //System.out.println("-------------------------- ");
        return count;
    }
}
