/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.DTInteractionWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joemullen
 */
public class MergingALLPairs {

    private Set<String> pairs = new HashSet<String>();
    private Set<String> p1 = new HashSet<String>();
    private Set<String> p2 = new HashSet<String>();
    private Set<String> p3 = new HashSet<String>();
    private Set<String> p4 = new HashSet<String>();
    private Set<String> presentInAll = new HashSet<String>();
    private Set<String> db3Pairs = new HashSet<String>();
    private Set<String> subgraphmatchedPairs = new HashSet<String>();
    private Set<String> uniquePairs = new HashSet<String>();


    public void mergeAllUniqueFromTheseFiles(String [] files) throws FileNotFoundException, IOException {

        Set<String> presentInAll = new HashSet<String>();

        BufferedWriter bw = new BufferedWriter(new FileWriter("Results/DB3WORK/ALLMATCHES_from4Subs.txt"));
//        String CCT = "/Users/joemullen/Desktop/Results For Paper/SemSearch Results/CCT/OPUNIQUPAIRS.txt";
//        String CTT = "/Users/joemullen/Desktop/Results For Paper/SemSearch Results/CTT/OPUNIQUPAIRS.txt";
//        String CIT = "/Users/joemullen/Desktop/Results For Paper/SemSearch Results/CIT/OPUNIQUPAIRS.txt";
//        String CTCT = "/Users/joemullen/Desktop/Results For Paper/SemSearch Results/CTCT/OPUNIQUPAIRS.txt";
//
//        String[] files = new String[4];
//        files[0] = CCT;
//        files[1] = CTT;
//        files[2] = CIT;
//        files[3] = CTCT;

        for (int i = 0; i < files.length; i++) {
            String file = files[i];

            BufferedReader br = new BufferedReader(new FileReader(file));
            String line2;
            try {
                while ((line2 = br.readLine()) != null) {
                    //System.out.println(line2);
                    String[] split = line2.split("\t");
                    String db = split[0];
                    String uni = split[1];
                    String p = db + "\t" + uni;
                    if (i == 0) {
                        p1.add(p);
                    }
                    if (i == 1) {
                        p2.add(p);
                    }
                    if (i == 2) {
                        p3.add(p);
                    }
                    if (i == 3) {
                        p4.add(p);
                    }

                    if (!pairs.contains(p)) {


                        pairs.add(db + "\t" + uni);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MergingALLPairs.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

       // System.out.println("Subgraphs identified: " + pairs.size() + " unique pairs");
        for (String p : pairs) {
            bw.append(p + "\n");
        }

        for (String p : p1) {

            if (p2.contains(p) && p3.contains(p) && p4.contains(p)) {
                presentInAll.add(p);
            }

        }
        for (String h : presentInAll){
        
            System.out.println(h);
        }

        System.out.println("PresentInAll: "+ presentInAll.size());
        bw.close();

    }

    private void getNoneMatchesFromTwoFiles() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("/Users/joemullen/Desktop/UniqueValidNonMatchedPairs"));
        BufferedReader db3valid = new BufferedReader(new FileReader("/Users/joemullen/Desktop/Comparing2/DB3 Unique/all_Valid.txt"));
        BufferedReader subgraphIdentified = new BufferedReader(new FileReader("/Users/joemullen/Desktop/Comparing2/MappingPairsFromSubgraphs2DB3valid/totalNOrepeatsallbutSmallTarg.txt"));

        String line;

        while ((line = db3valid.readLine()) != null) {
            db3Pairs.add(line);

        }
        String line2;

        while ((line2 = subgraphIdentified.readLine()) != null) {
            subgraphmatchedPairs.add(line2);
        }

        for (String p : db3Pairs) {
           // System.out.println(p);
            if (!subgraphmatchedPairs.contains(p)) {
                uniquePairs.add(p);
            }


        }

        for (String p : uniquePairs) {
            bw.append(p + "\n");

        }

        System.out.println("DB3 pairs: " + db3Pairs.size());
        System.out.println("SubgraphMatchesUnique: " + subgraphmatchedPairs.size());
        System.out.println("UniqueValidNonMatchedPairs: " + uniquePairs.size());
    }
}
