/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.DTInteractionWork;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author joemullen
 */
public class GetUniquePairs {

    Set<String> proteins = new HashSet<String>();
    Set<String> compounds = new HashSet<String>();
    ParseDrugBankData pi;

    public GetUniquePairs() throws FileNotFoundException, IOException {
        this.pi = new ParseDrugBankData();
        pi.getDatassetInfo();
        pi.getDrugBankV3Info();


    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        GetUniquePairs el = new GetUniquePairs();
        el.getCommonCompounds();
        el.getCommonProteins();
        el.ExtractMissingElements();
    }

    public void getCommonCompounds() throws FileNotFoundException, IOException {

        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallCompoundsDATASET();;

        Set<String> alldb3 = pi.getallCompoundsDBv3();

        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                //System.out.println("Not in both: " + DBID);
                NOMATCH++;
            } else {
                MATCH++;
                compounds.add(DBID);
            }


        }
        System.out.println("i---------------------------Compounds");
        System.out.println("in db2:  " + alldb2.size());
        System.out.println("in db3:  " + alldb3.size());
        System.out.println("not in both total:  " + NOMATCH);
        System.out.println("in both total:  " + MATCH);


    }

    public void getCommonProteins() throws FileNotFoundException, IOException {
        int NOMATCH = 0;
        int MATCH = 0;

        Set<String> alldb2 = pi.getallProtiensDATASET();
        Set<String> alldb3 = pi.getallProteinsDBv3();


        for (String DBID : alldb3) {
            if (!alldb2.contains(DBID)) {
                //System.out.println("Not in both: " + DBID);
                NOMATCH++;
            } else {
                MATCH++;
                proteins.add(DBID);
            }


        }
        System.out.println("i---------------------------Targets");
        System.out.println("in db2:  " + alldb2.size());
        System.out.println("in db3:  " + alldb3.size());
        System.out.println("not in both total:  " + NOMATCH);
        System.out.println("in both total:  " + MATCH);

    }

    public void ExtractMissingElements() throws FileNotFoundException, IOException {

        int validPairs = 0;
        int invalidPairs = 0;
        BufferedReader br = new BufferedReader(new FileReader("/Users/joemullen/Desktop/Comparing2/DB3 Unique/nomatchesdb3.txt"));

        BufferedWriter bw = new BufferedWriter(new FileWriter("/Users/joemullen/Dropbox/SemSubSearch/semsubsearch/Results/DB3WORK/VALIDnomatchesdb3.txt"));

        String line;
        while ((line = br.readLine()) != null) {
            String[] split = line.split("\t");
            if (compounds.contains(split[0]) && proteins.contains(split[1])) {
                validPairs++;
                  bw.append(line + "\n");
            } else {

                invalidPairs++;
            }

        }

        bw.close();

        System.out.println("------------------Summary");
        System.out.println("valid pairs: " + validPairs);
        System.out.println("invalid pairs: " + invalidPairs);
    }
}
