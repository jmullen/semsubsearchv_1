/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package semsubalgorithm.testing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import semsubalgorithm.DTInteractionWork.GetShortestPath;
import semsubalgorithm.DTInteractionWork.GetUniquePairs;
import semsubalgorithm.QueryGraph;
import semsubalgorithm.QueryGraph.SemanticSub;
import semsubalgorithm.SemSearch;
import semsubalgorithm.SerialisedGraph;
import java.io.File;
import java.util.Arrays;
import semsubalgorithm.DTInteractionWork.MergingALLPairs;
import semsubalgorithm.DTInteractionWork.SubGraphMatches2Accession;
import semsubalgorithm.SourceGraph;

/**
 *
 * @author joemullen
 */
public class DrugBankWorkPaper {

    public static void main(String args[]) throws FileNotFoundException, IOException, ClassNotFoundException {

        DrugBankWorkPaper dbw = new DrugBankWorkPaper();
        dbw.run();

    }

    public void run() throws FileNotFoundException, IOException, ClassNotFoundException {
        
//        SerialisedGraph ser = new SerialisedGraph();
//        semsubalgorithm.SourceGraph source = ser.useSerialized("graph.data");
//        part1_GETUNIQUEPAIRS();
//        part2_GETSHORTESTPATHS(source);
//        Set<QueryGraph> subs = part3_SEARCHFORSUBS(source);
//        part4_CONVERTMATCHES2DTPAIRS(subs);
        part5_MAPTODB3();
        part6_CROSSMATCH();

    }

    public void part1_GETUNIQUEPAIRS() throws FileNotFoundException, IOException {
        GetUniquePairs gup = new GetUniquePairs();
        gup.getCommonCompounds();
        gup.getCommonProteins();
        gup.ExtractMissingElements();
        System.out.println("[INFO] Calculated all unique pairs from DB3");

    }

    public void part2_GETSHORTESTPATHS(SourceGraph source) throws IOException {
        GetShortestPath sp = new GetShortestPath(source);
        sp.getPairs("Results/DB3WORK/VALIDnomatchesdb3.txt");
        sp.getAccessions("Results/DB3WORK/OP_tab_exporter/con_listOP_55e0c.tsv");
        sp.convertToUndirected();
        sp.getAllShortestPaths();
        System.out.println("[INFO] Got the shortest path betwen all valid DB3 pairs");

    }

    public Set<QueryGraph> part3_SEARCHFORSUBS(SourceGraph source) throws IOException {

        Set<QueryGraph> subs = new HashSet<QueryGraph>();
        QueryGraph q1 = new QueryGraph(SemanticSub.SIMTARGSSMALL);
        QueryGraph q2 = new QueryGraph(SemanticSub.CTIT);
        QueryGraph q3 = new QueryGraph(SemanticSub.CTCT);
        QueryGraph q4 = new QueryGraph(SemanticSub.SIMCOMPSSMALL);
        QueryGraph q5 = new QueryGraph(SemanticSub.NEWTARGIND);

        String dir1 = "Results/Searches/";
        File dir = new File(dir1);
        if (dir.isDirectory()) {
             for (File child : dir.listFiles()) {
                 if(child.delete()){
    			System.out.println(child.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation failed- check for duplicates with differing timestamps");
    		}
            }       
        }
       	
        subs.add(q1);
        //subs.add(q2);
        subs.add(q3);
        subs.add(q4);
        subs.add(q5);

        for (QueryGraph q : subs) {

            SemSearch sem = new SemSearch(source, q, 6, "Compound", true, 0.8, true, true, true);
            sem.completeSearch();
        }

        System.out.println("[INFO] Completed a semantic search for all semantic subgraphs");
        return subs;

    }

    public void part4_CONVERTMATCHES2DTPAIRS(Set<QueryGraph> subs) throws FileNotFoundException, IOException {
        String dir1 = "Results/Searches/";
        File dir = new File(dir1);
        if (dir.isDirectory()) {
            for (File child : dir.listFiles()) {
                String filenam = child.toString();
                if (filenam.contains("[")) {
                    String[] subName1 = filenam.split("\\[");
                    String name1 = subName1[1];
                    String[] split2 = name1.split("\\]_");
                    String subName = split2[0];

                    for (QueryGraph q : subs) {
                        if (q.getMotifName().equals(subName)) {
                            SubGraphMatches2Accession mm = new SubGraphMatches2Accession(filenam, q);
                            mm.mapp();
                        }

                    }
                }
            }
        }

        System.out.println("[INFO] Extracted the DB accesssion and Uniprot ID from every match (adn removed duplication)");
    }

    public void part5_MAPTODB3() throws FileNotFoundException, IOException {
        String[] files = new String[4];
        String dir2 = "Results/DB3WORK/UniquePairs/";
        File dir3 = new File(dir2);
        int count = 0;
        if (dir3.isDirectory()) {
            for (File child : dir3.listFiles()) {
                files[count] = child.toString();
                count++;
            }
        }

        MergingALLPairs mp = new MergingALLPairs();
        mp.mergeAllUniqueFromTheseFiles(files);
        System.out.println("[INFO] Identified all pairs that are identified by all semantic subgraphs");
    }
    
    public void part6_CROSSMATCH(){
    
    
    
    }
}
