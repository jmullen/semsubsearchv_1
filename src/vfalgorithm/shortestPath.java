/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author joemullen
 */
public class shortestPath {

    private DirectedGraph<String, DefaultEdge> targ;
    private SourceGraph sp;
    private Map<String, String> DDBID2ID;
    private Map<String, String> UNIPROTID2ID;
    private ArrayList<String> PAIRS;
    private int found = 0;
    private int nullpath = 0;
    private int greaterthan10 = 0;
    String file;
    private Map<String, Integer> commonPaths;
    private int noIDs = 0;

    public shortestPath(SourceGraph sg) throws IOException {

        this.sp = sg;
        this.targ = sg.createSourceGraph();
        this.PAIRS = new ArrayList<String>();
        this.DDBID2ID = new HashMap<String, String>();
        this.UNIPROTID2ID = new HashMap<String, String>();
        this.file = "/Users/joemullen/Desktop/pathsFINAL.txt";
        this.commonPaths = new HashMap<String, Integer>();

    }

    public static void main(String[] args) throws IOException {

        SourceGraph source = new SourceGraph(new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/con_listOP_55e0c.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/rel_listOP_55e0c.tsv"));
        shortestPath sp = new shortestPath(source);
        sp.getPairs("/Users/joemullen/Desktop/Comparing2/DB3 Unique/UniqueValidNonMatchedPairsMinusSubgraphPairs");
        sp.getAccessions("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/con_listOP_55e0c.tsv");
        sp.getAllShortestPaths();


    }

    public void getAccessions(String file) throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        br.readLine();
        while ((line = br.readLine()) != null) {
            String[] split = line.split("\t");
            if (!split[4].equals("NO_DRUGBANK")) {
                DDBID2ID.put(split[4], split[0]);
                System.out.println("Added DB: " + split[0] + ":" + split[4]);
            }

            if (!split[5].equals("NO_UNIPROT")) {
                UNIPROTID2ID.put(split[5], split[0]);
                System.out.println("Added UNi: " + split[0] + ":" + split[5]);
            }



        }
        // System.out.println(DDBID2ID.get("DB01656"));
    }

    public void getPairs(String file) throws FileNotFoundException, IOException {

        BufferedReader br = new BufferedReader(new FileReader(file));

        String line;
        while ((line = br.readLine()) != null) {
            PAIRS.add(line);

        }
    }

    public void getAllShortestPaths() throws IOException {

        BufferedWriter br = new BufferedWriter(new FileWriter(file));

        for (String pair : PAIRS) {
            System.out.println("--------------------------------");
            String DB = null;
            String UNIPROT = null;
            String[] split = pair.split("\t");
            if (split.length < 2) {
                System.out.println("MISSING INFO: " + DB + "  " + UNIPROT);
                
            } else {
                if (split[0].equals("Not Available") || split[1].equals("Not Available")) {
                    System.out.println("COULD NOT PARSE PAIR- one or more elements are missing");
                    noIDs++;
                } else {
                    if (DDBID2ID.containsKey(split[0])) {
                        DB = DDBID2ID.get(split[0]);

                    }
                    if (UNIPROTID2ID.containsKey(split[1])) {
                        UNIPROT = UNIPROTID2ID.get(split[1]);
                    }
                    if (DB != null && UNIPROT != null) {
                        System.out.println("Searching for path between db" + DB + " uni " + UNIPROT);
                        br.append(DB + "\t" + UNIPROT + "\t" + getShortestPath(DB, UNIPROT) + "\n");
                        found++;
                    } else {
                        System.out.println("COULD NOT FIND IDs FOR: " + split[0] + "  or: " + split[1]);
                        noIDs++;
                    }
                }
            }
        }

        br.append("\n");
        br.append("SUMMARY" + "\n");
        br.append("------------------------------" + "\n");
        br.append("Searched for " + PAIRS.size() + " drug target interactions" + "\n");
        br.append("------------------------------" + "\n");
        br.append("No of different graphs found: " + commonPaths.size() + "\n");
        br.append("Total sucessful finds: " + found + "\n");
        br.append("------------------------------" + "\n");
        br.append("No IDs (failed at parsing pairs): " + noIDs + "\n");
        br.append("Nullpaths (unsuccesful): " + nullpath + "\n");
        br.append("Paths greater than 10 (not included): " + greaterthan10 + "\n");
        br.append("------------------------------" + "\n");
        br.append("\n");


        Set<String> graphs = commonPaths.keySet();
        for (String graph : graphs) {
            commonPaths.get(graph);
            br.append(graph + "\t" + commonPaths.get(graph) + "\n");
        }

        br.close();

    }

    public String getShortestPath(String node1, String node2) {

        System.out.println("[INFO] Calling getShortestPath for: " + node1 + "  " + node2);
        //System.out.println("Alledges node1: " + targ.edgesOf(node1).size());
        //System.out.println("Incomingedges node1: " + targ.incomingEdgesOf(node1).size());
        //System.out.println("Outgoingedges node1: " + targ.outgoingEdgesOf(node1).size());
        //System.out.println("Alledges node2: " + targ.edgesOf(node2).size());
        //System.out.println("Incomingedges node2: " + targ.incomingEdgesOf(node2).size());
        //System.out.println("Outgoingedges node2: " + targ.outgoingEdgesOf(node2).size());

        if (targ.containsVertex(node1) && targ.containsVertex(node2)) {
            DijkstraShortestPath tr2;
            tr2 = new DijkstraShortestPath(targ, node1, node2);
            //System.out.println("TR2 to string: " + tr2.toString());
            //System.out.println("TR2 path length: " + tr2.getPathLength());

            String relations = "";
            String nodes = "";
            String conceptClass = "";

            if (tr2 == null) {
                //System.out.println("THE EDGES FOUND ARE EMPTY.... why??? Could not calculate the shortest path");
                nullpath++;

            } else if (tr2.getPathLength() > 10) {

                greaterthan10++;


            } else if (tr2 != null && tr2.getPathLength() < 10) {

                Object[] why;
                //if (tr2.getPathEdgeList().isEmpty()) {
//                    System.out.println("TR2 pathedgelist is empty!!");
                //              } else {
                why = tr2.getPathEdgeList().toArray();


                for (int i = 0; i < why.length; i++) {
                    String wh = why[i].toString();
                    String wh2 = wh.substring(1, wh.length() - 1);


                    String[] split = wh2.split(":");
                    if (i == why.length - 1) {
                        nodes = nodes + split[0].trim() + "\t" + split[1].trim();
                        conceptClass = conceptClass + "\t" + sp.getConceptClass(split[0].trim()) + "\t" + sp.getConceptClass(split[1].trim());
                    } else if (i == 0) {
                        nodes = split[0];
                        conceptClass = sp.getConceptClass(split[0].trim());
                    } else {
                        nodes = nodes + "\t" + split[0];
                        conceptClass = conceptClass + "\t" + sp.getConceptClass(split[0].trim());


                    }
                    relations = relations + why[i].toString();
                }

                relations.trim();
                nodes.trim();
                conceptClass.trim();
                System.out.println(nodes);
                System.out.println(conceptClass);
                System.out.println(relations);

                if (commonPaths.containsKey(conceptClass)) {
                    int local = commonPaths.get(conceptClass);
                    commonPaths.put(conceptClass, (local + 1));
                } else {
                    commonPaths.put(conceptClass, 1);
                }

                return conceptClass;

                //      }
            }
        }

        return "Could Not Get Shortest Path- too large (>10)";

    }
}
