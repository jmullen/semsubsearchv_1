///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package vfalgorithm;
//
//import java.io.BufferedWriter;
//import java.io.FileNotFoundException;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Set;
//import org.jgrapht.DirectedGraph;
//import org.jgrapht.alg.DijkstraShortestPath;
//import org.jgrapht.graph.DefaultDirectedGraph;
//import org.jgrapht.graph.DefaultEdge;
//import vfalgorithm.QueryGraph.SemanticSub;
//
///**
// *
// * @author joemullen
// */
//public class TopWithMultiple extends SourceGraph {
//    // use the ORIGINAL subgraph to find the initial nodes.
//    //then use this to start BOTH searches!!!
//
//    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
//        QueryGraph q = new QueryGraph(SemanticSub.FIVENODE);
//        //DirectedGraph<String, DefaultEdge> sub = q.fiveNodeMotif();
//        SplittingLargeSubs spli = new SplittingLargeSubs(q);
//        Set<DirectedGraph<String, DefaultEdge>> newSubs = spli.getNewSubs();
//        SerialisedGraph ser = new SerialisedGraph();
//        SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/VFAlgorithm/myobject.data");
//        DirectedGraph<String, DefaultEdge> tar = source.getSourceGraph();
//
//        for (DirectedGraph<String, DefaultEdge> sub2 : newSubs) {
//            System.out.println("Starting search");
//            long startTime = System.currentTimeMillis();
//      //      TopWithMultiple ts = new TopWithMultiple(sub2, tar, sub);
////            ts.completeSearch();
//
//            long endTime = System.currentTimeMillis();
//            System.out.println("[INFO] Topsearch serliazed Took " + (endTime - startTime) / 1000 + " seconds");
//        }
//    }
//
//    // constructor
//    public TopWithMultiple(DirectedGraph<String, DefaultEdge> sub,
//            DirectedGraph<String, DefaultEdge> tar, DirectedGraph<String, DefaultEdge> origi) throws IOException {
//
//        motif = sub;
//        targ = tar;
//        this.origi = origi;
//        origisubMap = nameEdgeset(origi);
//        subMap = nameEdgeset(sub);
//        targeyMap = nameEdgeset(tar);
//        initialNodes = startingNodes(mostConnectedTop(origisubMap),
//                subMap, targeyMap);
//        subGraphSize = getGraphSize(motif);
//        tarGraphSize = getGraphSize(targ);
//        sub_core = new String[subGraphSize];
//        sub_order = new String[subGraphSize];
//        tar_core = new String[subGraphSize];
//        sub_out = new String[subGraphSize];
//        sub_in = new String[subGraphSize];
//        tar_out = new String[tarGraphSize];
//        tar_in = new String[tarGraphSize];
//        potentialNodesPairsP = new String[tarGraphSize];
//        allMatches2 = new ArrayList<String>();
//
//    }
//    //need a constructor that takes a specified candidate set- look at the topSearch class!! merge them both into the same class- if a certain collection is empty we will do a topololgical search otherwise we will do a semantic search!!
//    int tempCount = 0;
//    private DirectedGraph<String, DefaultEdge> targ;
//    private DirectedGraph<String, DefaultEdge> motif;
//    private DirectedGraph<String, DefaultEdge> origi;
//    private HashMap<String, int[]> subMap;
//    private HashMap<String, int[]> origisubMap;
//    private HashMap<String, int[]> targeyMap;
//    private int subGraphSize;
//    private int tarGraphSize;
//    private String[] sub_core;
//    private String[] tar_core;
//    private String[] UID_core;
//    private String[] sub_order;
//    private String[] temp_new_additions;
//    private int subOrderCount = 0;
//    private int recursionCount = 0;
//    private String[] sub_out;
//    private String[] sub_in;
//    private String[] tar_out;
//    private String[] tar_in;
//    private String[] nextResukst;
//    private int UID = 0;
//    private int STATE = 0;
//    private int allMatchesExclSmallWorldAss = 0;
//    private String[] potentialNodesPairsP;
//    private String[][] allMatches;
//    private ArrayList<String> allMatches2;
//    private ArrayList<String> initialNodes;
//    private DirectedGraph<String, DefaultEdge> subTree = new DefaultDirectedGraph<String, DefaultEdge>(
//            DefaultEdge.class);
//    private DirectedGraph<String, DefaultEdge> UIDTree = new DefaultDirectedGraph<String, DefaultEdge>(
//            DefaultEdge.class);
//    private String rootNodeSub = "";
//    private String rootNodeUID = "";
//
//    // for each initial pair we need to return an array list of strings- next
//    // pairs
//    // then we can call the method that takes the array list in a while loop,
//    // taking the output of the previous
//    //need to think of a way to get the 2 graphs together!!
//    public String[][] completeSearch() throws IOException {
//
//       long startTime = System.currentTimeMillis();
//        recursiveVF2(initialNodes);
//         long endTime = System.currentTimeMillis();
//         System.out.println("Search took: "+ (endTime -startTime) /1000);
//
//        String[][] local = new String[allMatches2.size()][subGraphSize];
//        int popCount = 0;
//        for (String match : allMatches2) {
//            String[] here = match.split("\t");
//            for (int g = 0; g < here.length; g++) {
//                local[popCount][g] = here[g];
//            }
//            popCount++;
//        }
//
//        allMatches = local;
//
//        return local;
//    }
//
//    public void recursiveVF2(ArrayList<String> initialPairs) {
//
//        prepareDataStructures();
//
//        int numberCheckked = 1;
//
//        for (String pairs : initialPairs) {
//            if (allMatches2.size() == 5) {
//                break;
//
//            }
//            recursionCount = 0;
//
//            subTree = new DefaultDirectedGraph<String, DefaultEdge>(
//                    DefaultEdge.class);
//            UIDTree = new DefaultDirectedGraph<String, DefaultEdge>(
//                    DefaultEdge.class);
//
//            resetDataStructures();
//
//            STATE = 0;
//
//            String[] split = pairs.split(",,,");
//
//            String sub = split[0];
//            String tar = split[1];
//
//            rootNodeSub = sub.trim();
//            rootNodeUID = tar.trim();
//
//            // searchTree.addVertex(rootNode);
//            subTree.addVertex(rootNodeSub);
//            UIDTree.addVertex(rootNodeUID);
//
//            //System.out
//            //       .println("-----------------------------------------------STARTING SEARCH WITH CANDIDATE PAIR: "
//            //     + sub + " " + tar + ".......   (" + numberCheckked + " of " + initialNodes.size() + ")");
//
//            numberCheckked++;
//
//            String[] resultsSTATE1 = getNextNodes(sub, tar);
//
//            recursionCount++;
//
//            nextResukst = getNextNodes(resultsSTATE1);
//
//        }
//
//    }
//
//    public String[] getNextNodes(String subNode, String tarNode) {
//
//        resetDataStructures();
//
//        String[][] info = popUID_Core(rootNodeUID, tarNode);
//
//
//        sub_core = popSub_Core(rootNodeSub, subNode);
//
//        UID_core = info[0];
//
//        tar_core = info[1];
//
//
//        STATE = getState();
//
//        String allNodesAdded = "";
//
//        populateArrays(sub_core, tar_core);
//
//        calculateP();
//
//        for (int x = 0; x < potentialNodesPairsP.length - 1; x++) {
//            String pairs = potentialNodesPairsP[x];
//
//            if (pairs.equals("NULL_NODE")) {
//
//                break;
//            } else {
//                String[] split = pairs.split(",,,");
//                String sub = split[0].trim();
//                String tart = split[1].trim();
//                String[] target = removeUID(tart);
//                String tar = target[0].trim();
//
//
//                int count = 0;
//
//                if ((feas_2(sub, tar) == true)
//                        && (repeatabiltyCheck(tar, tar_core) == 0 && repeatabiltyCheck(
//                        sub, sub_core) == 0)
//                        && (feas_1(sub, tar) == true) && (smallWorld(sub_core) == true)) {
//
//                    allNodesAdded = allNodesAdded + pairs + "\t";
//                    //String nodeUID = tar.trim() + "===" + (recursionCount + 1);
//                    UIDTree.addVertex(tart);
//
//                    // only need to add the sub node ONCE!!!
//                    if (count == 0) {
//                        subTree.addVertex(sub.trim());
//                    }
//
//                    STATE = getState();
//
//                    UIDTree.addEdge(UID_core[STATE - 1].trim(), tart);
//
//                    if (count == 0) {
//                        subTree.addEdge(sub_core[STATE - 1].trim(), sub.trim());
//
//                    }
//
//                    if (STATE == subGraphSize - 1) {
//
//                        String[] match1 = new String[subGraphSize];
//                        String match = "";
//
//                        for (int f = 0; f < match1.length; f++) {
//
//                            if (f < subGraphSize - 1) {
//                                match1[f] = tar_core[f];
//                                match = match + tar_core[f] + "\t";
//                                if (subOrderCount == 0) {
//                                    sub_order[f] = sub_core[f];
//
//                                }
//
//                            } else {
//                                match1[f] = tar;
//                                match = match + tar;
//                                if (subOrderCount == 0) {
//                                    sub_order[f] = sub;
//
//                                }
//                            }
//
//                        }
//
//                        subOrderCount++;
//
//                        if (smallWorld(match1) == true) {
//                            if (allMatches2.contains(match)) {
//                                //System.out.println("already added");
//                                tempCount++;
//                            } else {
//
//                                allMatches2.add(match);
//                                // System.out.println("match!"
//                                //       + (Arrays.deepToString(match1)));
//                                allMatchesExclSmallWorldAss++;
//                            }
//
//                        }
//
//                    } else {
//
//                        //System.out.println("failed closed world assumption: ");
//                        allMatchesExclSmallWorldAss++;
//
//                    }
//                    count++;
//
//                } else {
//                    //System.out.println("Searchtree exhausted");
//                }
//            }
//        }
//
//        String[] allNodesAddedSplit = allNodesAdded.split("\t");
//        if (!allNodesAddedSplit[0].equals("")) {
//        } else {
//            String[] temp = new String[]{"Nothing"};
//            allNodesAddedSplit = temp;
//        }
//
//        return allNodesAddedSplit;
//
//    }
//
//    public String[] getNextNodes(String[] otherNodes) {
//
//        String newAdditions = "";
//
//        for (int u = 0; u < otherNodes.length; u++) {
//
//            String local = otherNodes[u];
//
//            String[] split = local.split(",,,");
//            String sub = split[0].trim();
//            if (sub.equals("Nothing")) {
//                break;
//            }
//            String tar = split[1].trim();
//
//            String[] local2 = getNextNodes(sub, tar);
//            for (int t = 0; t < local2.length; t++) {
//                newAdditions = newAdditions + local2[t].trim() + "\t";
//            }
//        }
//
//        String[] newAdditions2 = newAdditions.split("\t");
//
//        temp_new_additions = newAdditions2;
//
//        recursionCount++;
//
//        // System.out.println("these are added at recursion: "+recursionCount+" "+
//        // Arrays.deepToString(temp_new_additions));
//        //System.out.println("done check for recursioncount: " + recursionCount);
//        //System.out.println("new_additions_length: " + temp_new_additions.length);
//        while (recursionCount < subGraphSize - 1
//                && temp_new_additions.length > 1
//                && !temp_new_additions[0].equals("Nothing")) {
//            getNextNodes(temp_new_additions);
//        }
//
//        return newAdditions2;
//
//    }
//
//    public String addUID(String node) {
//        String unique = node + "===" + UID;
//        UID++;
//        return unique;
//    }
//
//    public String[] removeUID(String node) {
//        String[] split1 = node.split("===");
//        String[] split = new String[2];
//        for (int p = 0; p < split1.length; p++) {
//            split[p] = split1[p].trim();
//        }
//        return split;
//
//    }
//
//    public String[][] popUID_Core(String nodeInQuestion, String targetNode) {
//        //System.out.println(nodeInQuestion + "  " + targetNode + "  "
//        //+ UIDTree.toString());
//        DijkstraShortestPath tr2 = new DijkstraShortestPath(UIDTree,
//                nodeInQuestion, targetNode);
//
//        Object[] why = tr2.getPathEdgeList().toArray();
//        String[][] output = new String[2][subGraphSize];
//        String[] tempTar = new String[subGraphSize];
//        if (why.length > 0) {
//            String nodes = "";
//            for (int g = 0; g < why.length; g++) {
//
//                String relation1 = why[g].toString();
//                // System.out.println("pop UID-COre:" + relation1);
//                String relation2 = relation1.trim();
//                String relation = relation2
//                        .substring(1, relation2.length() - 1);
//
//                String[] split = relation.split(":");
//                // System.out
//                // .println("pop UID-COre:" + Arrays.deepToString(split));
//                for (int y = 0; y < split.length; y++) {
//                    String node = split[y].trim();
//                    if (g == 0) {
//                        nodes = nodes + node + "\t";
//                        // System.out.println("g (should be 0):" + g);
//                    } else if (y == 0) {
//                        nodes = nodes + split[1].trim() + "\t";
//                        // System.out.println("g (should be >0: " + g);
//                    }
//                }
//
//            }
//            // System.out.println("NODES: " + nodes);
//
//            String[] core = nodes.split("\t");
//            for (int h = 0; h < core.length; h++) {
//                if (!core[h].equals("")) {
//                    tempTar[h] = core[h];
//                }
//            }
//            for (int f = 0; f < tempTar.length; f++) {
//                if (tempTar[f] == null) {
//                    tempTar[f] = "NULL_NODE";
//                }
//            }
//        } else {
//            for (int f = 0; f < tempTar.length; f++) {
//                if (f == 0) {
//                    tempTar[f] = nodeInQuestion;
//                } else {
//                    tempTar[f] = "NULL_NODE";
//                }
//
//            }
//        }
//
//        output[0] = tempTar;
//        for (int y = 0; y < tempTar.length; y++) {
//            if (output[0][y].equals("NULL_NODE")) {
//                output[1][y] = output[0][y];
//            } else {
//                String[] local = removeUID(output[0][y]);
//                output[1][y] = local[0];
//
//            }
//        }
//        return output;
//    }
//
//    public String[] popSub_Core(String nodeInQuestion, String targetNode) {
//
//        DijkstraShortestPath tr2 = new DijkstraShortestPath(subTree,
//                nodeInQuestion, targetNode);
//
//        Object[] why = tr2.getPathEdgeList().toArray();
//
//        String[] tempTar = new String[subGraphSize];
//        if (why.length > 0) {
//            String nodes = "";
//            for (int g = 0; g < why.length; g++) {
//
//                String relation1 = why[g].toString();
//                // System.out.println("pop Sub-COre:" + relation1);
//                String relation2 = relation1.trim();
//                String relation = relation2
//                        .substring(1, relation2.length() - 1);
//
//                String[] split = relation.split(":");
//                // System.out
//                // .println("pop Sub-COre:" + Arrays.deepToString(split));
//                for (int y = 0; y < split.length; y++) {
//                    String node = split[y].trim();
//                    if (g == 0) {
//                        nodes = nodes + node + "\t";
//                        // System.out.println("g (should be 0):" + g);
//                    } else if (y == 0) {
//                        nodes = nodes + split[1].trim() + "\t";
//                        // System.out.println("g (should be >0: " + g);
//                    }
//                }
//
//            }
//            // System.out.println("NODES: " + nodes);
//
//            String[] core = nodes.split("\t");
//            for (int h = 0; h < core.length; h++) {
//                if (!core[h].equals("")) {
//                    tempTar[h] = core[h];
//                }
//            }
//            for (int f = 0; f < tempTar.length; f++) {
//                if (tempTar[f] == null) {
//                    tempTar[f] = "NULL_NODE";
//                }
//            }
//        } else {
//            for (int f = 0; f < tempTar.length; f++) {
//                if (f == 0) {
//                    tempTar[f] = nodeInQuestion;
//                } else {
//                    tempTar[f] = "NULL_NODE";
//                }
//
//            }
//        }
//        // need to fill the rest with "null_nodes"?!?!?!
//        // System.out.println("Sub_core fter pop:" +
//        // Arrays.deepToString(tempTar));
//        return tempTar;
//    }
//
//    public boolean feas_1(String subNode, String tarNode) {
//
//        boolean well = false;
//
//        if (STATE == 0) {
//
//            well = true;
//        } else {
//            if ((R_pred(subNode, tarNode) == true)
//                    && (R_succ(subNode, tarNode) == true)) {
//
//                well = true;
//
//            } else {
//                //System.out.println("failed feas_1");
//            }
//        }
//
//        return well;
//
//    }
//
//    public boolean R_pred(String sub, String tar) {
//        // predecessors (COMING IN)
//        // we want to know if the pairs match in the same way to the previous
//        // nodes of the partial mapping
//        boolean well = true;
//        String[] inComingNodes = tIn(motif, sub);
//
//        if (inComingNodes.length > 0) {
//
//            // get all outgoing nodes if the sub node of the pair
//
//            // System.out.println("R_pred 1 of sub : "
//            // + Arrays.deepToString(inComingNodes));
//            // finds the location within sub_core if the nodes have already been
//            // added to the partial mapping
//
//            ArrayList<Integer> location_in_mapping = new ArrayList<Integer>();
//
//            for (String node : inComingNodes) {
//                for (int i = 0; i < sub_core.length - 1; i++) {
//                    if (sub_core[i].equals(node)) {
//                        location_in_mapping.add(i);// up to here!!
//                    } else {
//                        // we have none to look at
//                        well = true;
//                        // System.out.println("R_pred 2: nothing to look at");
//                    }
//                }
//            }
//
//            for (int look : location_in_mapping) {
//                // System.out.println("looking for an edge between:  "
//                // + tar_core[look] + "  " + tar);
//                if (targ.containsEdge(tar_core[look].trim(), tar.trim()) == true) {
//                    well = true;
//                    //System.out.println("passed R_pred");
//                } else {
//                    well = false;
//                    //System.out.println("failed R_pred");
//                }
//            }
//        }
//
//        return well;
//
//    }
//
//    public boolean R_succ(String sub, String tar) {
//        // successors (GOING OUT)
//        boolean well = true;
//        // get all outgoing nodes if the sub node of the pair
//        String[] outGoingNodes = tOut(motif, sub);
//
//        if (outGoingNodes.length > 0) {
//            // finds the location within sub_core if the nodes have already been
//            // added to the partial mapping
//
//            ArrayList<Integer> location_in_mapping = new ArrayList<Integer>();
//
//            for (String node : outGoingNodes) {
//                for (int i = 0; i < sub_core.length - 1; i++) {
//                    if (sub_core[i].equals(node)) {
//                        location_in_mapping.add(i);// up to here!!
//                    } else {
//                        // we have none to look at
//                        well = true;
//                        // System.out.println("R_succ 2: nothing to look at");
//                    }
//                }
//            }
//            // System.out.println("locations of edges: " + location_in_mapping);
//            for (int look : location_in_mapping) {
//                if (targ.containsEdge(tar.trim(), tar_core[look].trim()) == true) {
//                    well = true;
//                    //System.out.println("passed R_succ");
//
//                } else {
//                    // System.out.println("no edge between tr:" + tar
//                    // + "tar look: " + tar_core[look]);
//                    well = false;
//
//                    //System.out.println("failed R_succ");
//
//                }
//
//            }
//        }
//
//        return well;
//    }
//
//    public boolean feas_2(String subNode, String tarNode) {
//
//        // System.out.println("feas_2: sub= " + subNode + "  tar= " + tarNode);
//
//        if (STATE == 0) {
//            return true;
//
//        } else {
//            int[] subEdgeset = subMap.get(subNode);
//            int[] tarEdgeset = targeyMap.get(tarNode);
//            return (R_termin(subEdgeset, tarEdgeset));
//        }
//
//    }
//
//    public boolean R_termin(int[] sub, int[] tar) {
//
//        boolean well = true;
//
//        for (int p = 0; p < sub.length; p++) {
//
//            if (tar[p] < sub[p]) {
//                well = false;
//                //System.out.println("failed feas_2");
//            }
//        }
//        return well;
//    }
//
//    public boolean R_termout(String sub) {
//
//        return false;
//
//    }
//
//    public void feas_3() {
//        // will call R_new
//    }
//
//    public boolean R_new() {
//
//        return false;
//
//    }
//
//    public void resetDataStructures() {
//        for (int i = 0; i < subGraphSize; i++) {
//            if (sub_core[i].equals("NULL_NODE")) {
//                break;
//
//            } else {
//                sub_core[i] = "NULL_NODE";
//            }
//        }
//        for (int i = 0; i < subGraphSize; i++) {
//            if (tar_core[i].equals("NULL_NODE")) {
//                break;
//
//            } else {
//
//                tar_core[i] = "NULL_NODE";
//            }
//        }
//        for (int i = 0; i < subGraphSize; i++) {
//            if (sub_out[i].equals("NULL_NODE")) {
//                break;
//
//            } else {
//                sub_out[i] = "NULL_NODE";
//            }
//        }
//        for (int i = 0; i < subGraphSize; i++) {
//            if (sub_in[i].equals("NULL_NODE")) {
//                break;
//
//            } else {
//                sub_in[i] = "NULL_NODE";
//            }
//        }
//        for (int i = 0; i < tarGraphSize; i++) {
//            if (tar_out[i].equals("NULL_NODE")) {
//                break;
//
//            } else {
//                tar_out[i] = "NULL_NODE";
//            }
//        }
//        for (int i = 0; i < tarGraphSize; i++) {
//            if (tar_in[i].equals("NULL_NODE")) {
//                break;
//
//            } else {
//                tar_in[i] = "NULL_NODE";
//            }
//        }
//        for (int i = 0; i < tarGraphSize; i++) {
//            if (potentialNodesPairsP[i].equals("NULL_NODE")) {
//                break;
//
//            } else {
//                potentialNodesPairsP[i] = "NULL_NODE";
//            }
//        }
//    }
//
//    public void prepareDataStructures() {
//        for (int i = 0; i < subGraphSize; i++) {
//            sub_core[i] = "NULL_NODE";
//            tar_core[i] = "NULL_NODE";
//            sub_out[i] = "NULL_NODE";
//            sub_in[i] = "NULL_NODE";
//        }
//
//        for (int i = 0; i < tarGraphSize; i++) {
//            tar_out[i] = "NULL_NODE";
//            tar_in[i] = "NULL_NODE";
//            potentialNodesPairsP[i] = "NULL_NODE";
//
//        }
//
//    }
//
//    public void populateArrays(String[] subnode, String[] tarnode) {
//
//        String[] tar_out1;
//        tar_out1 = tOut(targ, tarnode);
//
//        for (int i = 0; i < tar_out1.length; i++) {
//
//            tar_out[i] = addUID(tar_out1[i]);
//        }
//        String[] tar_in1;
//        tar_in1 = tIn(targ, tarnode);
//
//        for (int i = 0; i < tar_in1.length; i++) {
//            tar_in[i] = addUID(tar_in1[i]);
//        }
//
//        String[] sub_out1;
//        sub_out1 = tOut(motif, subnode);
//        for (int i = 0; i < sub_out1.length; i++) {
//            sub_out[i] = sub_out1[i];
//        }
//
//        String[] sub_in1;
//        sub_in1 = tIn(motif, subnode);
//        for (int i = 0; i < sub_in1.length; i++) {
//            sub_in[i] = sub_in1[i];
//        }
//    }
//
//    public String calculateP() {
//        String mostConnectednode = "";
//        int highest = 0;
//
//        if (!tar_out[0].equals("NULL_NODE") && !sub_out[0].equals("NULL_NODE")) {
//
//            for (int y = 0; y < sub_out.length - 1; y++) {
//                String local = sub_out[y];
//                if (!sub_out[y].equals("NULL_NODE")) {
//                    int[] local2 = subMap.get(local);
//
//
//                    if ((local2[0] >= highest)
//                            && (repeatabiltyCheck(local, sub_core) == 0)) {
//                        highest = local2[0];
//                        mostConnectednode = local;
//
//                    }
//
//                }
//            }
//
//            for (int i = 0; i < tar_out.length; i++) {
//                if (tar_out[i].equals("NULL_NODE")) {
//
//                    break;
//                } else if (!mostConnectednode.equals("")) {
//
//                    potentialNodesPairsP[i] = mostConnectednode + ",,,"
//                            + tar_out[i];
//
//                }
//
//            }
//        }
//
//        if (mostConnectednode.equals("") && !tar_in[0].equals("NULL_NODE")
//                && !sub_in[0].equals("NULL_NODE")) {
//
//            String mostConnectednodeIn = "";
//            int highestIn = 0;
//
//            for (int y = 0; y < sub_in.length - 1; y++) {
//
//                String local = sub_in[y];
//                if (!sub_in[y].equals("NULL_NODE")) {
//
//                    int[] local2 = subMap.get(local);
//
//                    if ((local2[0] >= highestIn)
//                            && (repeatabiltyCheck(local, sub_core) == 0)) {
//                        highestIn = local2[0];
//                        mostConnectednodeIn = local;
//
//                    }
//                }
//            }
//
//            for (int i = 0; i < tar_in.length; i++) {
//
//                if (tar_in[i].equals("NULL_NODE")) {
//
//                    break;
//                } else {
//
//                    potentialNodesPairsP[i] = mostConnectednodeIn + ",,,"
//                            + tar_in[i];
//
//                }
//            }
//
//
//        } else {
//            //System.out.println("no more nodes to explore");
//            // this is where we would look at all the remaining node
//        }
//        return mostConnectednode;
//
//    }
//
//    public boolean completeMatch() {
//        boolean completeMatch = false;
//
//        if (getGraphSize(targ) == getGraphSize(motif)) {
//            completeMatch = true;
//
//        }
//        return completeMatch;
//
//    }
//
//    public int getState() {
//
//        int county = 0;
//        for (int y = 0; y < sub_core.length; y++) {
//            if (sub_core[y] == "NULL_NODE") {
//                county++;
//            }
//
//        }
//        int myPredictedState = (sub_core.length - county);
//
//        return myPredictedState;
//
//    }
//
//    public String[][] getRelations(DirectedGraph<String, DefaultEdge> sub)
//            throws IOException {
//
//        String[][] relations = new String[allMatches2.size()][sub_order.length];
//        if (allMatches2.size() == 0) {
//        } else {
//
//            for (int y = 0; y < allMatches.length; y++) {
//                if (allMatches[y][0] == null) {
//
//                    break;
//
//                } else {
//                    String[] local = allMatches[y];
//                    String subEdge = sub.edgeSet().toString();
//                    for (int u = 0; u < sub_order.length; u++) {
//                        subEdge = subEdge.replaceAll(sub_order[u], local[u]);
//
//                    }
//
//                    String[] subedge2 = subEdge.substring(1,
//                            subEdge.length() - 1).split(", ");
//                    for (int h = 0; h < subedge2.length; h++) {
//                        subedge2[h] = subedge2[h].trim().substring(1,
//                                subedge2[h].length() - 1);
//
//                    }
//                    relations[y] = subedge2;
//                }
//
//            }
//        }
//
//        // System.out.println(Arrays.deepToString(relations));
//        return relations;
//
//    }
//
//    public int repeatabiltyCheck(String node, String[] contained) {
//        int ma = 0;
//        if (STATE > 0) {
//
//            for (int i = 0; i <= contained.length - 1; i++) {
//                String here = contained[i];
//                if (node.equals(here)) {
//                    ma++;
//                }
//            }
//
//        } else {
//
//            ma = 0;
//        }
//        // System.out.println("repeatabilty check: " + node + " score: " + ma);
//        return ma;
//    }
//
//    public void printOutMatches() {
//        int count = 0;
//        for (String match : allMatches2) {
//            count++;
//            System.out.println("Match No. " + count + " " + match);
//        }
//        if (count == 0) {
//
//            System.out.println("No matches found");
//        }
//
//        System.out.println("Sub_order: " + Arrays.deepToString(sub_order));
//        System.out.println("Matches Failing Closed World: "
//                + (allMatchesExclSmallWorldAss - count));
//
//    }
//
//    public void produceFile(String[][] nodesOfMatches2,
//            String[][] conceptNames, String[][] conceptClasses2,
//            String[][] relationsOfMatches2, String[][] relationTypes2)
//            throws IOException {
//
//        FileWriter fstream = new FileWriter("matchesWSubGSize_" + subGraphSize
//                + ".txt");
//        BufferedWriter out = new BufferedWriter(fstream);
//
//        for (int y = 0; y < nodesOfMatches2.length; y++) {
//            if (y == allMatches.length - 1) {
//                break;
//            }
//            if (nodesOfMatches2[y][0] == null) {
//                break;
//            } else {
//                try {
//                    out.append(">>> " + (y + 1) + "\n");
//
//                    String[] localNodes = nodesOfMatches2[y];
//                    out.append("SN:");
//                    for (int t = 0; t < localNodes.length; t++) {
//                        out.append(localNodes[t] + "\t");
//                    }
//                    out.append("\n");
//
//                    String[] localConcepts = conceptNames[y];
//                    out.append("NN:");
//                    for (int t = 0; t < localConcepts.length; t++) {
//                        out.append(localConcepts[t] + "\t");
//                    }
//                    out.append("\n");
//
//                    String[] localClasses = conceptClasses2[y];
//                    out.append("CC:");
//                    for (int t = 0; t < localClasses.length; t++) {
//                        out.append(localClasses[t] + "\t");
//                    }
//                    out.append("\n");
//
//                    String[] localReltype = relationsOfMatches2[y];
//                    out.append("SR:");
//                    for (int t = 0; t < localReltype.length; t++) {
//                        out.append(localReltype[t] + "\t");
//                    }
//                    out.append("\n");
//
//                    String[] localRel = relationTypes2[y];
//                    out.append("RT:");
//                    for (int t = 0; t < localRel.length; t++) {
//                        out.append(localRel[t] + "\t");
//                    }
//                    out.append("\n");
//
//                } catch (Exception e) {
//                    System.out.println("Error: " + e.getMessage());
//
//                }
//
//            }
//        }
//
//        out.close();
//
//    }
//
//    public String removeUsedEdges(String[] nodes) {
//        System.out.println("entering removeUsedEdges: "
//                + Arrays.deepToString(nodes));
//        String lastConnection = "";
//        for (int j = 1; j < nodes.length; j++) {
//
//            if (nodes[j].equals("NULL_NODE")) {
//                break;
//            }
//            String node = nodes[j];
//
//            Set<DefaultEdge> edges = UIDTree.edgesOf(node);
//            if (edges.size() > 2) {
//                System.out.println("delete all edges after this node: " + node);
//                lastConnection = node;
//                System.out.println("remove edge between: " + node + "and  "
//                        + nodes[j - 1]);
//                UIDTree.removeEdge(node, nodes[j - 1]);
//                UIDTree.removeVertex(nodes[j - 1]);
//                System.out.println("removed the vertex: " + nodes[j - 1]);
//                break;
//            }
//
//        }
//
//        return lastConnection;
//
//    }
//
//    public int getNumberMatches() {
//        return allMatches.length;
//
//    }
//
//    public boolean smallWorld(String[] match) {
//
//        boolean test = true;
//
//        for (int u = 0; u < sub_order.length; u++) {
//            if (sub_order[u] == null) {
//                break;
//            }
//            for (int i = 0; i < sub_order.length; i++) {
//                if (sub_order[i] == null) {
//                    break;
//                }
//                if ((motif.containsEdge(sub_order[u], sub_order[i]) == false
//                        && (motif.containsEdge(sub_order[i], sub_order[u]) == false) && ((targ
//                        .containsEdge(match[u], match[i]) == true) || (targ
//                        .containsEdge(match[i], match[u]) == true)))) {
//                    test = false;
//                }
//
//            }
//
//        }
//        return test;
//    }
//
//    public boolean relationNotPresent(DirectedGraph<String, DefaultEdge> graph,
//            String v1, String v2) {
//        boolean hasRelation = false;
//        if (graph.containsEdge(v1, v2)) {
//            hasRelation = true;
//        }
//
//        return hasRelation;
//    }
//
//    public ArrayList<String> getAllMatches() {
//
//        return allMatches2;
//
//    }
//
//    public String getOrder() {
//        
//        String or="";
//        
//        for (int i = 0; i< sub_order.length; i++){
//            or = or+ sub_order[i]+ "\t";
//        
//        }
//        or.trim();
//
//        return or;
//    }
//}