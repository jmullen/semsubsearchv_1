///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package vfalgorithm;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Set;
//import org.jgrapht.DirectedGraph;
//import org.jgrapht.graph.DefaultEdge;
//
///**
// *
// * @author joemullen
// */
//public class TopMultipleRun {
//
//    String[] correctOrder;
//    String[] ordersub1;
//    String suborder1;
//    String suborder2;
//    String[] ordersub2;
//    String[] wrongOrder;
//    Set<DefaultEdge> subEdges;
//    DirectedGraph<String, DefaultEdge> tar;
//    DirectedGraph<String, DefaultEdge> sub;
//    ArrayList<String> potentialMacthes = new ArrayList<String>();
//
//    public TopMultipleRun(SplittingLargeSubs s, DirectedGraph<String, DefaultEdge> tar, QueryGraph q) throws IOException {
//        //Set<DirectedGraph<String, DefaultEdge>> indisubs = s.getNewSubs();
//        DirectedGraph<String, DefaultEdge> originalSub = q.getSub();
//        ArrayList<String> matches1 = new ArrayList<String>();
//        //String suborder1 = "";
//        ArrayList<String> matches2 = new ArrayList<String>();
//        // suborder2 = "";
//        this.tar = tar;
//        this.subEdges = originalSub.edgeSet();
//        this.sub = originalSub;
//        Object[] corder = q.getSub().vertexSet().toArray();
//        String[] local = new String[corder.length];
//        for (int i = 0; i < corder.length; i++) {
//            local[i] = corder[i].toString();
//
//        }
//
//        correctOrder = local;
//        System.out.println(Arrays.deepToString(corder));
//
//        int count = 0;
//        for (DirectedGraph<String, DefaultEdge> grap : indisubs) {
//            TopWithMultiple tm2 = new TopWithMultiple(grap, tar, originalSub);
//            tm2.completeSearch();
//            count++;
//            if (count == 1) {
//                matches1 = tm2.getAllMatches();
//                suborder1 = tm2.getOrder();
//            }
//            if (count == 2) {
//
//                matches2 = tm2.getAllMatches();
//                suborder2 = tm2.getOrder();
//            }
//
//        }
//
//        System.out.println("-----------------------matches1, ORDER " + suborder1);
//        ordersub1 = suborder1.split("\t");
////        for (String mat : matches1) {
////            System.out.println(mat);
////        }
//
//        System.out.println("-----------------------matches2, ORDER " + suborder2);
//        ordersub2 = suborder2.split("\t");
////        for (String mat : matches2) {
////            System.out.println(mat);
////        }
//
//        orderInSubs();
//        getPotentialSubs(matches1, matches2);
//        checkPotentialSubs();
//
//
//
//    }
//
//    private String[] orderInSubs() {
//
//
//        String o2 = "";
//        for (int i = 0; i < ordersub2.length; i++) {
//            System.out.println("suborder: " + ordersub2[i]);
//            if (i > 0) {
//                o2 = o2 + ordersub2[i] + "\t";
//            }
//        }
//
//        o2.trim();
//        String local = suborder1 + o2;
//        String[] split = local.split("\t");
//
//        wrongOrder = split;
//
//        return split;
//
//    }
//
//    private boolean checkTwoArrays(String[] one, String[] two) {
//        boolean check = true;
//
//        for (int i = 1; i < one.length; i++) {
//            for (int o = 1; o < two.length; o++) {
//                if (one[i].equals(two[o])) {
//                    check = false;
//                }
//
//            }
//
//        }
//
//        return check;
//
//    }
//
//    private void getPotentialSubs(ArrayList<String> one, ArrayList<String> two) {
//
//        //ArrayList<String> potentialMacthes = new ArrayList<String>();
//
//        for (String ssub : one) {
//            String[] split = ssub.split("\t");
//            for (String ssub2 : two) {
//                String[] split2 = ssub2.split("\t");
//                if (split[0].equals(split2[0])) {
//                    if (checkTwoArrays(split, split2) == true) {
//                        String match = ssub;
//                        for (int i = 1; i < split2.length; i++) {
//                            match = match + "\t" + split2[i];
//
//                        }
//                        potentialMacthes.add(convertToCorrectOrder(match));
//
//                    }
//
//                }
//
//            }
//
//        }
//
//        System.out.println("PotentialMatches: " + potentialMacthes.size());
//        //return potentialMacthes;
//    }
//
//    private void checkPotentialSubs() {
//        ArrayList<String> matches = new ArrayList<String>();
//        for (String match : potentialMacthes) {
//            boolean check = true;
//            String[] split = match.split("\t");
//
//            for (DefaultEdge sedge : subEdges) {
//                String from = sub.getEdgeSource(sedge);
//                String to = sub.getEdgeTarget(sedge);
//                int frommy = 100;
//                int tooey = 100;
//                for (int i = 0; i < correctOrder.length; i++) {
//                    if (correctOrder[i].equals(from)) {
//                        frommy = i;
//                    }
//
//                    if (correctOrder[i].equals(to)) {
//                        tooey = i;
//                    }
//                }
//
//                if (!tar.containsEdge(split[frommy], split[tooey])) {
//                    check = false;
//                    //potentialMacthes.remove(match);
//                    break;
//
//                }
//
//
//            }
//
//            if (check == true) {
//
//                if (closedWorld(split) == true) {
//                    matches.add(match);
//                    //potentialMacthes.remove(match);
//                }
//
//            }
//        }
//        System.out.println("Matches After Checks " + matches.size());
//
//
//    }
//
//    public boolean closedWorld(String[] match) {
//
//        boolean test = true;
//
//        for (int u = 0; u < correctOrder.length; u++) {
//
//            for (int i = 0; i < correctOrder.length; i++) {
//          
//                if ((sub.containsEdge(correctOrder[u], correctOrder[i]) == false
//                        && (sub.containsEdge(correctOrder[i], correctOrder[u]) == false) && ((tar
//                        .containsEdge(match[u], match[i]) == true) || (tar
//                        .containsEdge(match[i], match[u]) == true)))) {
//                    test = false;
//                }
//
//            }
//
//        }
//        return test;
//    }
//
//    private String convertToCorrectOrder(String match) {
//        String[] split = match.split("\t");
//        String rightOrder = "";
//        //String[] rightOrder = new String [split.length];
//        for (int i = 0; i < correctOrder.length; i++) {
//            for (int y = 0; y < correctOrder.length; y++) {
//                if (correctOrder[i].equals(wrongOrder[y])) {
//                    rightOrder = rightOrder + split[y] + "\t";
//                }
//            }
//
//        }
//        return rightOrder;
//    }
//}
