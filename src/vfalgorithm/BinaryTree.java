/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author joemullen
 */
public class BinaryTree {

    Node root;

    public void addNode(int key, SplitInstance s) {


        //create a new node and initialize it

        Node newNode = new Node(key, s);
        //if there is no root this becomes root
        if (root == null) {
            root = newNode;

        } else {
            //Set root as the Node we will start
            // with as we traverse the graph
            Node focusNode = root;

            //Future parent for out new node

            Node parent;

            while (true) {
                //root is the top parent so we start there
                parent = focusNode;
                //check if the new node should go on the ledt side of the new parent node
                if (focusNode == null) {
                    //then place the new node on the left of it
                    parent.leftChild = newNode;
                    return;


                } else {//if we get here put the node on the right
                    focusNode = focusNode.rightChild;

                    //if the right child has no children
                    if (focusNode == null) {

                        //then place the new node on the right of it
                        parent.rightChild = newNode;
                        return;

                    }

                }


            }



        }


    }

//all nodes are visited in ascending order
//recusrion is used to go to one node and then
//go to its child nodes and so forth
    public void inOrderTraverseTree(Node focusNode) {

        if (focusNode != null) {
//traverse the left node

            inOrderTraverseTree(focusNode.leftChild);

//cisit the currently focussed node
            System.out.println(focusNode);

//traverse to the right
            inOrderTraverseTree(focusNode.rightChild);




        }


    }

    public Set<SplitInstance> getLeafNodes(Node focusNode) {
        Set<SplitInstance> leaves = new HashSet<SplitInstance>();
        if (focusNode == null) {
            return leaves;
        }
        if (focusNode.leftChild == null && focusNode.rightChild == null) {
            System.out.println(focusNode);
            leaves.add(focusNode.getSplitInstance());
        } else {
            leaves.add(focusNode.leftChild.getSplitInstance());
            leaves.add(focusNode.rightChild.getSplitInstance());
            getLeafNodes(focusNode.leftChild);
            getLeafNodes(focusNode.rightChild);
        }

        return leaves;
    }

    public void preOrderTraverseTree(Node focusNode) {

        if (focusNode != null) {


            System.out.println(focusNode);

            //traverse the right Node
            preOrderTraverseTree(focusNode.rightChild);
            preOrderTraverseTree(focusNode.leftChild);

        }


    }

    public void postOrderTraverseTree(Node focusNode) {

        if (focusNode != null) {

            postOrderTraverseTree(focusNode.leftChild);
            postOrderTraverseTree(focusNode.rightChild);

            System.out.println(focusNode);

        }

    }

    public void semSearchTree(Node focusNode, SourceGraph source, double threshold) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        if (focusNode != null) {

            SplitSearch ss2 = new SplitSearch(focusNode.getSplitInstance(), source, threshold);
            ss2.run();
            semSearchTree(focusNode.leftChild, source, threshold);

            semSearchTree(focusNode.rightChild, source, threshold);




            // System.out.println(focusNode);

        }

    }

    public int findKey(SplitInstance s, Node focusNode) {

        int key = 0;
        if (focusNode != null) {


            postOrderTraverseTree(focusNode.leftChild);
            if (focusNode.getSplitInstance().equals(s)) {
                key = focusNode.getKey();
                //break;

            }
            postOrderTraverseTree(focusNode.rightChild);
            if (focusNode.getSplitInstance().equals(s)) {
                key = focusNode.getKey();
                //break;

            }
            //System.out.println(focusNode);

        }
        return key;
    }

    public Node findNodeKey(int key) {

        //start at the top of the tree
        Node focusNode = root;

        //whle we haven't found the node keep looking

        while (focusNode.key != key) {

            //if we should search to the left

            if (key < focusNode.key) {
                //shift focus to the node on the left
                focusNode = focusNode.leftChild;

            } else {
                //shift focus to the righ child node
                focusNode = focusNode.rightChild;
            }


            if (focusNode == null) //the node wasn't found
            {
                return null;
            }



        }

        return focusNode;
    }

    public Node findNode(int key) {

        //start at the top of the tree
        Node focusNode = root;

        //whle we haven't found the node keep looking

        while (focusNode.key != key) {

            //if we should search to the left

            if (key < focusNode.key) {
                //shift focus to the node on the left
                focusNode = focusNode.leftChild;

            } else {
                //shift focus to the righ child node
                focusNode = focusNode.rightChild;
            }


            if (focusNode == null) //the node wasn't found
            {
                return null;
            }



        }

        return focusNode;
    }

    public static void main(String[] args) {

        BinaryTree theTree = new BinaryTree();

//        theTree.addNode(50, "Boss");
//        theTree.addNode(25, "Vice president");
//        theTree.addNode(15, "Office manager");
//        theTree.addNode(30, "Secretary");
//        theTree.addNode(75, "Sales Manager");
//        theTree.addNode(85, "Salesman 1");
//
//        System.out.println("\nNode with the key 75");
//        System.out.println(theTree.findNode(75));
//
//        System.out.println("traverse");
//        theTree.inOrderTraverseTree(theTree.root);
//        System.out.println("preOrder");
//        theTree.preOrderTraverseTree(theTree.root);



    }
}

class Node {

    int key;
    SplitInstance s;
    Node leftChild;
    Node rightChild;

    Node(int key, SplitInstance s) {
        this.key = key;
        this.s = s;

    }

    public SplitInstance getSplitInstance() {

        return s;
    }

    public int getKey() {

        return key;

    }

    @Override
    public String toString() {
        return s.toString() + " has the key: " + key;

    }
}
