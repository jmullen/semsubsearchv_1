/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author joemullen
 */
public class GraphPruning {

    QueryGraph q;
    SourceGraph s;
    SemanticDistanceCalculator sdc;
    private DirectedGraph<String, DefaultEdge> target;
    private DirectedGraph<String, DefaultEdge> newGraph;
    DirectedGraph<String, DefaultEdge> query;
    Set<String> ConceptClassesToInclude = new HashSet<String>();
    float threshold;
    //counts for testing
    int deletedNodes = 0;
    int deletedEdges = 0;
    String summary;
    long startTime;
    long endTime;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {
        //SerialisedGraph ser = new SerialisedGraph();
        //SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/VFAlgorithm/myobject.data");
        //QueryGraph query = new QueryGraph();
        // SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();
        // GraphPruning gp = new GraphPruning(source, query, sdc, 0.8);
    }

    // do we need to update the Semantics?? Think not, would just take time.
    public GraphPruning(SourceGraph graph, QueryGraph q, SemanticDistanceCalculator SDC, double th) throws IOException, Exception {

        this.q = q;
        this.query = q.getSub();
        this.sdc = SDC;
        this.threshold = (float) th;
        this.s = graph;

        this.target = s.getSourceGraph();
        startTime = System.currentTimeMillis();
        getConceptClasses();
        Prune();
        checkAnyLoneNodes();
        endTime = System.currentTimeMillis();
        summary = summary + "Pruning Took " + (endTime - startTime) / 1000 + " seconds)";
        System.out.println(getSummary());




    }

    private void getConceptClasses() throws Exception {
        Set<String> allnodes = query.vertexSet();
        Object[] an = allnodes.toArray();
        Set<String> alloriginal = sdc.getAllConceptClasses();
        //for every concept class in the subgraph
        for (int i = 0; i < an.length; i++) {
            String nod = an[i].toString();
            String conclass = q.getQueryNodeCC(nod);
            //score against every conceptclass in the graph
            //if greater than the threshold then include cc in graph
            for (String cc : alloriginal) {
                if (sdc.getNodeDistance(conclass, cc) > threshold) {
                    ConceptClassesToInclude.add(cc);
                }
            }
        }

        summary = "----------------------------------------------------------" + "\n" + "Pruning- ConceptClasses to be included: " + ConceptClassesToInclude.toString();


    }

    private void Prune() {
        newGraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);
        Set<String> allnodes = target.vertexSet();

        Object[] an = allnodes.toArray();

        //add nodes to the new graph

        for (int i = 0; i < an.length; i++) {
            String nod = an[i].toString();
            //if the conceptclass of the node is not to be included then we delete the node
            if (!ConceptClassesToInclude.contains(s.getConceptClass(nod))) {
                //we then delete all the edges associated with the node
                //Set<DefaultEdge> relatedEdges = target.edgesOf(nod);
                //target.removeAllEdges(relatedEdges);
                //deletedEdges += relatedEdges.size();
                //target.removeVertex(nod);
                //deletedNodes++;
            } else {

                newGraph.addVertex(nod);

            }

        }

        //add relations to the new graph
        Set<String> allnodesNew = newGraph.vertexSet();
        for (String node : allnodesNew) {
            //get the vertex set from the original graph
            Set<DefaultEdge> edgies = target.edgesOf(node);
            for (DefaultEdge ed : edgies) {
                if (allnodesNew.contains(target.getEdgeTarget(ed)) && allnodesNew.contains(target.getEdgeSource(ed))) {
                    newGraph.addEdge( target.getEdgeSource(ed), target.getEdgeTarget(ed));

                }
            }



        }

        

        summary = summary + " (Semantic Pruning) Deleted: " + (target.vertexSet().size() - newGraph.vertexSet().size()) + "/" + (target.edgeSet().size() - newGraph.edgeSet().size()) + " (nodes/edges),  New graph size : " + newGraph.vertexSet().size() + "/" + newGraph.edgeSet().size() + " (nodes/edges) ";
        //then submit this as the new graph for s
        //s.setSourceGraph(target);

    }

    private void checkAnyLoneNodes() {

        Set<String> allnodes = newGraph.vertexSet();
        Object[] an = allnodes.toArray();
        int deleted = 0;

        for (int i = 0; i < an.length; i++) {
            String nod = an[i].toString();
            if (newGraph.edgesOf(nod).size() == 0) {
                //System.out.println("This node has no edges and has been deleted: "+ nod);
                newGraph.removeVertex(nod);
                deleted++;
            }
        }

        summary = summary + " (No Relations) Deleted: " + deleted + " (nodes), New graph size : " + newGraph.vertexSet().size() + "/" + newGraph.edgeSet().size() + " (nodes/edges)";

    }

    public DirectedGraph<String, DefaultEdge> getPrunedGraph() throws IOException {
        return newGraph;
        //return new SourceGraph(target2, newsource.getAllCOnceptInfo());
    }

    public SourceGraph getPrunedSource() throws IOException {
        //SourceGraph sg = (SourceGraph) s.clone();
        SourceGraph sg = new SourceGraph(newGraph,s.getAllCOnceptInfo() );
        return sg;
        //return new SourceGraph(target2, newsource.getAllCOnceptInfo());
    }

   
    public String getSummary() {
        return summary;

    }

    public String getTime() {
        String time = (" " + (double)(endTime - startTime) / 1000);
        return time.trim();
    }
}
