/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

/**
 *
 * @author joemullen
 */
public class RandomQuery {

    private String[][] seminfo;
    int size;
    QueryGraph random;
    String[] ccsToPickFrom;
    private String startingConceptClass;
    boolean edges;
    long startTime;
    long endTime;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/VFAlgorithm/myobject.data");

        ArrayList<Integer> sizes = new ArrayList<Integer>();
        sizes.add(3);
        sizes.add(4);
        sizes.add(5);
        sizes.add(6);
        sizes.add(7);
        sizes.add(8);

        //sizes.add(10);
        for (Integer siz : sizes) {

            RandomQuery rq = new RandomQuery(source.getAverageConnectivityOfEachConceptClass(), siz, "Compound");


        }

    }

    public RandomQuery(String[][] seminfo, int size, String startingConceptClass) {

        this.seminfo = seminfo;
        this.size = size;
        this.startingConceptClass = startingConceptClass;
        getCCOccurremces();
        createRanQuery();
        if (size > 3) {
            while (random.getSub().vertexSet().size() >= random.getSub().edgeSet().size()) {
                System.out.println("calling createRan again--- not enough edges");
                createRanQuery();
            }
        }
        else if(size <4){
            while (random.getSub().vertexSet().size() > random.getSub().edgeSet().size()) {
                System.out.println("calling createRan again--- not enough edges");
                createRanQuery();
            }
        
        }
        System.out.println(getSummary());

    }

    private void createRanQuery() {

        startTime = System.currentTimeMillis();
        DirectedGraph<String, DefaultEdge> SemGraph = new SimpleDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        Map<String, String> queryNODEInfo = new HashMap<String, String>();

        int name = 0;
        String nodename = startingConceptClass + name;

        //we want to look for compounds in the graph- we start each random
        //query graph by adding a node of type compound
        SemGraph.addVertex(nodename);
        queryNODEInfo.put(nodename, startingConceptClass);
        name++;
        Random rand = new Random();
        //we start at one- have already added one node
        for (int i = 1; i < size; i++) {
            Set<String> nodes = SemGraph.vertexSet();
            String[] nodesinArray = new String[nodes.size()];
            int count = 0;
            for (String nod : nodes) {
                nodesinArray[count] = nod;
                count++;

            }

            //randomly pick a node from the graph to create a relation from
            int rando = rand.nextInt(nodes.size());
            String fromName = nodesinArray[rando];
            String fromCC = queryNODEInfo.get(fromName);


            String[] rules = new String[1];
            for (int p = 0; p < seminfo.length; p++) {
                if (seminfo[p][0].equals(fromCC)) {

                    rules = seminfo[p][3].split(",");
                    break;

                }

            }




            String[][] rulespruned = new String[rules.length][2];
            int count2 = 0;
            int total = 0;

            // System.out.println("Searchingsmeinfo: " + Arrays.deepToString(seminfo));

            if (rules.length < 1) {
                createRanQuery();


            }


            for (int y = 0; y < rules.length; y++) {

                for (int u = 0; u < seminfo.length; u++) {

                    String[] split = rules[y].split("=");
                    if (seminfo[u][0].equals(split[0].trim())) {


                        rulespruned[y][0] = split[0];
                        rulespruned[y][1] = split[1];
                        total += Integer.parseInt(split[1]);
                        count2++;

                    }
                }

            }



            int prunedcount = 0;

            String[] possibleCCs = new String[total];
            if (rulespruned.length < 2) {
                //createRanQuery();
                break;

            } else {

                for (int j = 0; j < rulespruned.length; j++) {

                    for (int h = 0; h < Integer.parseInt(rulespruned[j][1]); h++) {

                        possibleCCs[prunedcount] = rulespruned[j][0];
                        prunedcount++;


                    }
                }


            }

            int rando3 = rand.nextInt(prunedcount);
            String toCC = possibleCCs[rando3];

            toCC = possibleCCs[rando3];


            nodename = toCC + name;
            SemGraph.addVertex(nodename);
            queryNODEInfo.put(nodename, toCC);
            name++;

            SemGraph.addEdge(fromName, nodename);
            //check that there may be any symmetrical relations- if so need to add 
            //the closed world assumption will mean we miss any that do not include
            //this information
            if (checkForSymmetricalRelation(fromCC, toCC) == true) {
                SemGraph.addEdge(nodename, fromName);

            }


        }



        if (SemGraph.vertexSet().size() != size) {

            createRanQuery();
        } else {
            System.out.println("Created graph with:" + SemGraph.vertexSet().size() + "/" + SemGraph.edgeSet().size());
            String mname = "RANDOM(" + SemGraph.vertexSet().size() + "/" + SemGraph.edgeSet().size() + ")";
            random = new QueryGraph(SemGraph, mname, queryNODEInfo);
            endTime = System.currentTimeMillis();
        }




    }

    private boolean checkForSymmetricalRelation(String fromCC, String toCC) {
        boolean add = false;

        //get the rules for the toNode
        String[] rules = new String[0];
        for (int p = 0; p < seminfo.length; p++) {
            if (seminfo[p][0].equals(toCC)) {

                rules = seminfo[p][3].split(",");
                break;

            }

        }
        //if the rules contains the from CC then we want to add another edge
        if (rules.length > 0) {
            for (int u = 0; u < rules.length; u++) {
                String[] split = rules[u].split("=");
                if (split[0].trim().equals(fromCC)) {

                    add = true;
                }

            }
        }

        //System.out.println("add= " + add);
        return add;
    }

    private boolean rulesContainsCC(String[] rules, String cc) {
        boolean check = false;

        for (int y = 0; y < rules.length; y++) {
            if (rules[y].equals(cc)) {
                check = true;
            }

        }


        return check;
    }

    //returns an array of length 100, with the correct number (percentage) of each concept 
    //class as appears in the large graph
    private void getCCOccurremces() {

        String[] ccs = new String[100];
        int count = 0;
        for (int i = 0; i < seminfo.length; i++) {
            String conceptClass = seminfo[i][0];
            int frequency = Integer.parseInt(seminfo[i][1]);
            for (int y = 0; y < frequency; y++) {
                ccs[count] = conceptClass;
                count++;
            }


        }

        ccsToPickFrom = ccs;


    }

    public QueryGraph getRanQuery() {


        return random;

    }

    public String[][] getSemInfo() {

        return seminfo;
    }

    public String getSummary() {
        return "-----------------------------------------------------" + "\n" + "Random Query Graph Created and Contains: " + random.getSub().vertexSet().size() + " nodes, and: " + random.getSub().edgeSet().size() + " edges and took: " + (endTime - startTime) / 1000 + " seconds " + random.getSub().toString();
    }
}
