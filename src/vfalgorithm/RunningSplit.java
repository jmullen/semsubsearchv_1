/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author joemullen
 */
public class RunningSplit {

    public static void main(String args[]) throws IOException, FileNotFoundException, ClassNotFoundException {

       // QueryGraph q = new QueryGraph(QueryGraph.SemanticSub.EIGHTNODE);
          
        
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/VFAlgorithm/myobject.data");
        QueryGraph six = new RandomQuery(source.getAverageConnectivityOfEachConceptClass(), 6, "Compound").getRanQuery();
        
        RandomGraph rg = new RandomGraph(source.getGraphNodeSize(), source.getAverageConnectivityOfEachConceptClass(), 10000, 1);
        RunningSplit rs = new RunningSplit(six);
        rs.storeSplits();
        rg.createSemanticGraph();
        
        SemSearch sem = new SemSearch(rg.getRandomSourceGraph(), six, 6, "Compound", true, 0.8, true, false);
        sem.completeSearch();
       
        try {
            rs.searchAllSplits(rg.getRandomSourceGraph(), 0.8);
        } catch (Exception ex) {
            Logger.getLogger(RunningSplit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    QueryGraph orig;
    Set<SplitInstance> allsplits;
    BinaryTree bt;

    public RunningSplit(QueryGraph q) {

        this.orig = q;
        this.allsplits = new HashSet<SplitInstance>();
        this.bt = new BinaryTree();


    }

    public void storeSplits() {
        int run = 1;
        int key = 100;
        int leftKey = 50;
        int rightKey = 150;

        SplittingLargeSubs spl;
        SplitInstance ins;

        spl = new SplittingLargeSubs(orig);
        ins = spl.getSplit();
        //System.out.println(ins.toString());
        bt.addNode(key, ins);

        Set<SplitInstance> leaves = bt.getLeafNodes(bt.root);

        if (leaves.isEmpty()) {
            System.out.println("No leaves to explore");
        } else {
            for (SplitInstance s : leaves) {
                int key2 = bt.findKey(s, bt.root);
                if (s.checkSplitSizes() == true) {
                    System.out.println("Already reduced to its lowest form");
                } else {
                    if (ins.getQueryGraph1().getSub().vertexSet().size() > 4) {
                        SplittingLargeSubs spl2 = new SplittingLargeSubs(ins.getQueryGraph1());
                        SplitInstance ins2 = spl2.getSplit();
                        bt.addNode(key2 - (run * 10), ins2);

                    }

                    if (ins.getQueryGraph2().getSub().vertexSet().size() > 4) {
                        SplittingLargeSubs spl2 = new SplittingLargeSubs(ins.getQueryGraph2());
                        SplitInstance ins3 = spl2.getSplit();
                        bt.addNode(key2 + (run * 10), ins3);

                    }

                }
                run++;
            }
        }





        System.out.println("postOrder");
        bt.postOrderTraverseTree(bt.root);
//        System.out.println("Allsplit: " + bt.toString());
//        System.out.println("leafNodes");
//        bt.printLeafNodes(bt.root);
    }

    public void searchAllSplits(SourceGraph source, double threshold) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {

        bt.semSearchTree(bt.root, source, threshold);
    }
}
