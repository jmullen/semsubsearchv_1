/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author joemullen
 */
//@SuppressWarnings("serial")
public class SerialisedGraph implements java.io.Serializable {

    private static final long serialVersionUID = 384726404614661473L;
    String summary ="";
    long startTime;
    long endTime;

    public SerialisedGraph() throws IOException {

        super();
    }

    public static void main(String[] args) throws IOException, Exception {
        try {
            SourceGraph s = new SourceGraph(new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/con_listOP_55e0c.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/LargePLUSindsPLUSdisgenet/rel_listOP_55e0c.tsv"));
            //SourceGraph s = new SourceGraph();
            SerialisedGraph sg = new SerialisedGraph();
            sg.SerializeGraph(s);

        } catch (FileNotFoundException fe) {
            System.out.println(fe);

        }

    }

    public void SerializeGraph(SourceGraph s) throws FileNotFoundException, IOException {
       startTime = System.currentTimeMillis();
        try (FileOutputStream f_out = new FileOutputStream("myobject.data")) {
            ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
            //obj_out.writeObject(s);
            obj_out.writeObject(s);
            obj_out.close();
        }
        
        endTime = System.currentTimeMillis();
        
        System.out.printf("Serialized graph is saved as...");

    }

    public SourceGraph useSerialized(String file) throws FileNotFoundException, IOException, ClassNotFoundException {
        SourceGraph s = null;
        startTime = System.currentTimeMillis();

        try {
            FileInputStream f_in = new FileInputStream(file);
            ObjectInputStream obj_in = new ObjectInputStream(f_in);
            s = (SourceGraph) obj_in.readObject();
            f_in.close();
            obj_in.close();
            endTime = System.currentTimeMillis();
            return s;

        } catch (IOException i) {
            i.printStackTrace();

        }
        
        return s;

    }
    
    public String getSummary(){
    
     return "Serialized Graph Parsed in: " + (endTime - startTime) / 1000 + " seconds";
    }
}
