/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

/**
 *
 * @author joemullen
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;
import vfalgorithm.QueryGraph.SemanticSub;

public final class RandomGraph {

    //want to make sure that we create relations of the right proportion to the right concept class...
    //then delete that node from the candidate nodes... so as to not create the same relation twice!!!
    //need to improve this !!!!!! it itsn
    Set<String> nodes;
    ArrayList<String> relations;
    private DirectedGraph<String, DefaultEdge> random;
    String[][] ranSemInfo;
    SourceGraph source;
    long startTime;
    long endTime;
    String summary;
    boolean symmetrical = false;
    int sourceNodeSize;
    String[][] semInfo;
    int newSize;
    double connectivity;

    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/VFAlgorithm/myobject.data");
        System.out.println(source.getHighestConnectivity());
        System.out.println("nodes in source: " + source.getSourceGraph().vertexSet().size());
        System.out.println("edges in source: " + source.getSourceGraph().edgeSet().size());

       // RandomGraph rg = new RandomGraph(source.getGraphNodeSize(), source.getAverageConnectivityOfEachConceptClass(), 15000, 1);
        //rg.createSemanticGraph();
        //SourceGraph s2 = rg.getRandomSourceGraph();
        //System.out.println(Arrays.deepToString(s2.getAverageConnectivityOfEachConceptClass()));

//       QueryGraph q = new QueryGraph(SemanticSub.SIMCOMPSSMALL);
//       RandomQuery rq = new RandomQuery(source.getAverageConnectivityOfEachConceptClass(), 4, "Compound");
//       
//       RandomQuery five = new RandomQuery(source.getAverageConnectivityOfEachConceptClass(), 5, "Compound");
//       RandomQuery six = new RandomQuery(source.getAverageConnectivityOfEachConceptClass(), 6, "Compound");
//       
//       SourceGraph s3 = rg.getRandomSourceGraph();
//       SemSearch sem6 = new SemSearch(s2, rq.getRanQuery(), 1, "none", false, 0.8, false, false);
//        sem6.completeSearch();
//        SemSearch sem7 = new SemSearch(s2, five.getRanQuery(), 1, "none", false, 0.8, false, false);
//        sem7.completeSearch();
//        SemSearch sem8 = new SemSearch(s2, six.getRanQuery(), 1, "none", false, 0.8, false, false);
//        sem8.completeSearch();

    }

    public RandomGraph(int sourceNodeSize, String[][] semInfo, int newSize, double connectivity) {
        this.random = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);
        this.sourceNodeSize = sourceNodeSize;
        this.semInfo = semInfo;
        this.newSize = newSize;
        this.connectivity = connectivity;
    }

    final void createSemanticGraph() throws IOException {
        //System.out.println(Arrays.deepToString(semInfo));
        long startTime = System.currentTimeMillis();

        createNodes();

        //create the relations following the rules in the semInfo (relations can only go TO the conceptclasses in the array)
        Random rand = new Random();
        Set<String> nodes = random.vertexSet();
        int relcount = 0;
        for (String node : nodes) {

            String cc = ranSemInfo[Integer.parseInt(node) - 1][2];
            //System.out.println("CC from: " + cc);
            String from = node;
            String rules = getRules(cc);
            int numberOfOutGoingRels = getNumberOfOutGoingRels(cc);


            if (rules.isEmpty()) {
                //do nothing
            } else {

                String[][] splitRules = splitRules(rules);

                //for each rule we must create the number of relations expected
                //to that concept class
                // System.out.println("RULIES: " + Arrays.deepToString(splitRules));

                //go through each rulie of the ORIGINAL NODE
                for (int p = 0; p < splitRules.length; p++) {
                    //the first ruleTO
                    String toconclass = splitRules[p][0];



                    Set<String> alreadyrelated = new HashSet<String>();
                    //do we have symmetrical relations??
                    int number = (Integer.parseInt(splitRules[p][1]) / (int) connectivity);
                    String rulesTo = getRules(toconclass);

                    // System.out.println(rulesTo.length());

                    if (rulesTo.length() != 0) {
                        String[][] splitRulesTO = splitRules(rulesTo);
                        //get the rules for the TO conceptclass

                        symmetrical = false;

                        //go through each rule of the TO concept class
                        for (int u = 0; u < splitRulesTO.length; u++) {

                            String toFROMconceptclass = splitRulesTO[u][0];
                            //if the TO conceptClass we are looking at MATCHES one of its rules
                            if (toFROMconceptclass.equals(cc)) {

                                //minus the relations that the node already has to said conceptclass
                                //from the total we must create

                                Set<DefaultEdge> alreadyAdded = random.edgesOf(from);

                                int alreadyadded = 0;
                                for (DefaultEdge de : alreadyAdded) {
                                    String conceptClassTO = "";
                                    if (random.getEdgeSource(de).equals(from)) {
                                        conceptClassTO = ranSemInfo[Integer.parseInt(random.getEdgeTarget(de)) - 1][2];
                                        alreadyrelated.add(random.getEdgeTarget(de));
                                    }


                                    if (conceptClassTO.equals(toconclass)) {
                                        number = number - 1;
                                    }

                                }


                                symmetrical = true;
                            }


                        }



                        //first of all we create an array of those nodes that are in 
                        //the graph and match the concept class we are interested in
                        List<String> relevantNodes = new ArrayList<String>();
                        for (int h = 0; h < ranSemInfo.length; h++) {
                            if (ranSemInfo[h][2].equals(toconclass)) {
                                relevantNodes.add(ranSemInfo[h][0]);
                            }
                        }

                        for (String al : alreadyrelated) {
                            if (relevantNodes.contains(al)) {

                                relevantNodes.remove(al);
                                //System.out.println("deleted: " + al + " as they are already related");
                            }

                        }

                        //for loops not allowed
                        relevantNodes.remove(from);

                        //if we have already created all the edges as symmetrical edges no need to create

                        if (number > 0) {
                            for (int f = 0; f < number; f++) {

                                int randomNumber = rand.nextInt(relevantNodes.size());
                                String to = relevantNodes.get(randomNumber);
                                if (random.containsEdge(from, to)) {
                                    while (random.containsEdge(from, to)) {
                                        randomNumber = rand.nextInt(relevantNodes.size());
                                        to = relevantNodes.get(randomNumber);


                                    }


                                } else {

                                    random.addEdge(from, to);
                                    //  System.out.println("FROM: " + getRandomCC(from) + "TO: " + getRandomCC(to));
                                    //if symmetrical then add a return edge
                                    if (symmetrical == true) {

                                        random.addEdge(to, from);
                                        relcount++;


                                    }
                                    //don't want to create the same edge twice
                                    relevantNodes.remove(to);
                                    relcount++;

                                }
                                symmetrical = false;
                            }

                        }

                    }

                }

            }


        }


        long endTime = System.currentTimeMillis();

        source = new SourceGraph(random, ranSemInfo);
        int count =0;
        for (String node : source.getSourceGraph().vertexSet()) {
            if (source.getSourceGraph().edgesOf(node).size() == 0) {
                count ++;
            }

        }
        System.out.println("unconnected nodes= "+ count);
        System.out.println(getSummary());
    }

    public void createNodes() {
        String[][] newGraphInfo = new String[newSize][3];

        int nodeMultiply = newSize / 100;


        int nodeName = 0;
        //create nodes
        for (int i = 0;
                i < semInfo.length;
                i++) {

            String cc = semInfo[i][0];
            int number = Integer.parseInt(semInfo[i][1]) * nodeMultiply;
            for (int y = 0; y < number; y++) {
                random.addVertex(Integer.toString(nodeName + 1));
                //we do this so that we can use the same methods in Source Graph, which uses nodeIDs from Ondex which start at 1
                newGraphInfo[nodeName][0] = (Integer.toString(nodeName + 1));
                newGraphInfo[nodeName][1] = "Random";
                //format- source graph has cc in the [0][2]
                newGraphInfo[nodeName][2] = cc;
                nodeName++;

            }

        }

        ranSemInfo = newGraphInfo;


    }
    //connectivity is 10: if we put 2 in as an argument it is divided by 2 = 5

    public String getRules(String cc) {
        String rules = "";
        for (int i = 0; i < semInfo.length; i++) {
            if (semInfo[i][0].equals(cc)) {
                rules = semInfo[i][3];
                //numberOfOutGoingRels = (int) (((Integer.parseInt(semInfo[i][2]))) / connectivity);
                //System.out.println("Number of outgoring rels of " + cc + " : " + numberOfOutGoingRels);
            }
        }
        return rules;
    }

    public int getNumberOfOutGoingRels(String cc) {

        int rels = 0;
        for (int i = 0; i < semInfo.length; i++) {
            if (semInfo[i][0].equals(cc)) {

                rels = (int) (((Integer.parseInt(semInfo[i][2]))) / connectivity);
                //System.out.println("Number of outgoring rels of " + cc + " : " + numberOfOutGoingRels);
            }
        }
        return rels;

    }

    public String[][] splitRules(String rules) {
        // System.out.println("String rules: "+ rules);
        if (rules.endsWith(",")) {
            rules = rules.substring(0, rules.length() - 1);

        }
        String[] rul = rules.split(",");
        String[][] rulies = new String[rul.length][2];

        for (int i = 0; i < rul.length; i++) {

            String[] split = rul[i].split("=");
            String toconclass = split[0];
            String freq = split[1];
            rulies[i][0] = toconclass;
            rulies[i][1] = freq;
        }

        return rulies;

    }

    public SourceGraph getRandomSourceGraph() {

        return source;

    }

    public SourceGraph getCloneForEditing() {
        SourceGraph c = (SourceGraph) source.clone();
        return c;
    }

    public DirectedGraph<String, DefaultEdge> getRandomGraph() {

        return random;
    }

    public String[][] getSemInfo() {
        return ranSemInfo;

    }

    public String getSummary() {
        return "-----------------------------------------------------" + "\n" + "Random Graph Created and Contains: " + random.vertexSet().size() + " nodes, and: " + random.edgeSet().size() + " edges, and took: " + (endTime - startTime) / 1000 + " seconds";
    }

    public String getRandomCC(String node) {

        return ranSemInfo[Integer.parseInt(node) - 1][2];

    }
}
