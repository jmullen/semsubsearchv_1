/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

/**
 *
 * @author joemullen
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sjcockell
 * @version 0.1a
 */
public class SemanticDistanceCalculator {

    private HashMap<String, HashMap> edgeMap;
    private HashMap<String, HashMap> nodeMap;
    private Set<String> allCCs;
    private Set<String> allRTs;

    /**
     * Constructor
     */
    public SemanticDistanceCalculator() {
        this.allCCs =  new HashSet<String>();
        this.allRTs = new HashSet<String>();
        try {
            //Read edge distance matrix
            edgeMap = getMatrix("edge_distance.txt", "edges");
            //Read node distance matrix
            nodeMap = getMatrix("node_distance.txt", "nodes");
        } catch (IOException ioe) {
            Logger.getLogger(SemanticDistanceCalculator.class.getName()).log(Level.SEVERE, null, ioe);
        }
    }

    /**
     *
     * @param edge1
     * @param edge2
     * @return
     * @throws Exception
     */
    public float getEdgeDistance(String edge1, String edge2) throws Exception {
        //calculate semantic distance between edge1 and edge2
        return getDistance(edge1, edge2, edgeMap);
    }

    /**
     *
     * @param node1
     * @param node2
     * @return
     * @throws Exception
     */
    public float getNodeDistance(String node1, String node2) throws Exception {
        //calculate semantic distance between node1 and node2
        return getDistance(node1, node2, nodeMap);
    }

    /**
     *
     * @param type1
     * @param type2
     * @param map
     * @return
     * @throws Exception
     */
    private float getDistance(String type1, String type2, HashMap<String, HashMap> map) throws Exception {
        //calculate arbitrary distance between thing1 and thing2, given map
        //System.out.println("SDC between: " + type1+"  "+ type2);
        HashMap one = map.get(type1);
        Float score = (Float) one.get(type2);
        return score.floatValue();
    }

    /**
     *
     * @param file
     * @return
     * @throws IOException
     */
    private HashMap<String, HashMap> getMatrix(String file, String type) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        String header = br.readLine();
        StringTokenizer st = new StringTokenizer(header);
        ArrayList titles = new ArrayList();
        //create array of titles
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            titles.add(token);
        }
        if (type.equals("edges")) {
            for (Object edge : titles) {
                allRTs.add(edge.toString());
            }
        }
        if (type.equals("nodes")) {
            for (Object edge : titles) {
                allCCs.add(edge.toString());
            }
        }
        //System.out.println("titles " + titles.toString());
        // is this necessary?
        HashMap<String, HashMap> map = new HashMap<String, HashMap>();
        String line;
        while ((line = br.readLine()) != null) {
            st = new StringTokenizer(line);
            String currentTitle = st.nextToken();
            //System.out.println("currentTitle " + currentTitle);
            map.put(currentTitle, new HashMap<String, Float>());
            int i = 0;
            int length = st.countTokens();
            while (i < length) {
                String otherTitle = (String) titles.get(i);

                Float score = new Float(st.nextToken());

                HashMap temp = map.get(currentTitle);

                temp.put(otherTitle, score);


                i++;
            }

        }
        //System.out.println(map.toString());
        return map;
    }

    public Set<String> getAllConceptClasses() {
        return allCCs;
    }

    public Set<String> getAllRelationTypes() {
        return allRTs;
    }
}
