/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

/**
 *
 * @author joemullen
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sjcockell
 */
public class SemanticDistanceCalculatorTest {

    /**
     * 
     * @param args 
     */
    
    public static void main(String [] args) {
        SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();
        
        try {    
            //System.out.println("are we getting to here?");
            System.out.println(""+sdc.getEdgeDistance("sim", "has_similar_sequence"));
            System.out.println(""+sdc.getNodeDistance("Protein", "Compound"));
            System.out.println("All ccs :"+ sdc.getAllConceptClasses().toString());
            System.out.println("All rts :"+ sdc.getAllRelationTypes().toString());
            
        } catch (Exception ex) {
            Logger.getLogger(SemanticDistanceCalculatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


