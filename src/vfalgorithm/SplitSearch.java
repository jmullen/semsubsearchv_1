/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vfalgorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import vfalgorithm.QueryGraph.SemanticSub;

/**
 *
 * @author joemullen
 */
public class SplitSearch {

    QueryGraph q1;
    QueryGraph q2;
    SplitInstance si;
    Set<QueryGraph> allQueries;
    Set<QueryGraph> remainingQueries;
    QueryGraph originalSub;
    String getCrossoverNode;
    Set<DefaultEdge> extraEdges;
    ArrayList<String> allMatches1;
    ArrayList<String> allMatches2;
    SourceGraph source;
    SplittingLargeSubs split;
    String[] sub_order_1;
    String[] sub_order_2;
    String[] original_order;
    int[] orderConverter;
    ArrayList<String> mergedMatches;
    ArrayList<String> scoredMatches;
    SemanticDistanceCalculator sdc;
    Map<String, String> origiSem;
    double threshold;
    long startTime;
    long endTime;
    double searchTime;
    // Map<String, Set<DirectedGraph<String, DefaultEdge>>> allQuerySubMatches1;
    // Map<String, Set<DirectedGraph<String, DefaultEdge>>> allQuerySubMatches2;

    //get the edgeset of the most connected node as it is in the original subgraph
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException, Exception {
//        SerialisedGraph ser = new SerialisedGraph();
//        SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/VFAlgorithm/myobject.data");
//
//        // SourceGraph source = new SourceGraph(new File("/Users/joemullen/Desktop/OP_tab_exporter/Small Dataset/con_listOP_32bad.tsv"), new File("/Users/joemullen/Desktop/OP_tab_exporter/Small Dataset/rel_listOP_32bad.tsv"));
//
//        //RandomGraph sem = new RandomGraph();
//        //sem.createSemanticGraph(source.getGraphNodeSize(), source.getAverageConnectivityOfEachConceptClass(), 10000, 1);
//
//        QueryGraph q = new QueryGraph(SemanticSub.SIXNODE);
//        SemSearch sem11 = new SemSearch(source, q, 6, "Compound", true, 1.0, false, false);
//        sem11.completeSearch();
//        ArrayList<String> one = sem11.getAllMatches();
//
//        SplittingLargeSubs split = new SplittingLargeSubs(q);
//
//        SplitSearch spl = new SplitSearch(split, source, 1.0);
//        spl.run();
//        ArrayList<String> two = spl.getScoredMatches();
//
//        int mathc = 0;
//        int noneMatch = 0;
//        for (String mat : two) {
//            String[] split2 = mat.split("\t");
//            Set<String> ch = new HashSet<String>();
//            for (int u = 0; u < split2.length; u++) {
//                ch.add(split2[u]);
//
//            }
//            for (String mat2 : one) {
//                String[] split3 = mat.split("\t");
//                Set<String> ch2 = new HashSet<String>();
//                for (int u = 0; u < split3.length; u++) {
//                    ch2.add(split3[u]);
//
//                }
//                if (ch.equals(ch2)) {
//                    mathc++;
//
//                } else {
//                    noneMatch++;
//                }
//
//            }
//
//        }
//
//        System.out.println("In both: " + mathc + "  only in 2: " + noneMatch + " only in one: " + (one.size() - mathc));
        //System.out.println(splitrun.getSuccesfulInitialNodes());
    }

    public SplitSearch(SplitInstance s, SourceGraph source, double threshold) {
        this.si = s;
        this.originalSub = s.getOrig();
        this.getCrossoverNode = s.getCrossoverNode();
        this.q1 = s.getQueryGraph1();
        this.q2 = s.getQueryGraph2();
        this.source = source;

        this.mergedMatches = new ArrayList<String>();
        this.scoredMatches = new ArrayList<String>();
        this.sdc = new SemanticDistanceCalculator();
        this.origiSem = originalSub.getQueryNodeInfo();
        this.threshold = threshold;

//      

    }

    public void run() throws IOException, FileNotFoundException, ClassNotFoundException, Exception {


        SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();
        startTime = System.currentTimeMillis();


        VFSplit splitrun = new VFSplit(source, q1, getOrigiSub(), null, 4, "none", true, 0.8, true, false);
        splitrun.completeSearch();
        allMatches1 = splitrun.getAllMatches();
        // allQuerySubMatches1 = splitrun.getAllQuerySubMatches();
        // System.out.println(allQuerySubMatches1.toString());
        sub_order_1 = splitrun.getSubOrder();
        searchTime = Double.parseDouble(splitrun.getTime());
        


        // if we have no matches from the first search then there is no point continuing!
        if (allMatches1.size() == 0) {
            System.out.println("No matches found");

        } else {
            //need to convert the output of getsuccesfulintialnodes to ArrayList form

            VFSplit splitrun2 = new VFSplit(source, q2, getOrigiSub(), splitrun.getSuccesfulInitialNodes(), 4, "none", true, 0.8, true, false);
            splitrun = null;
            splitrun2.completeSearch();
            searchTime = searchTime + Double.parseDouble(splitrun2.getTime());

            allMatches2 = splitrun2.getAllMatches();
            //     allQuerySubMatches2 = splitrun.getAllQuerySubMatches();
            //    System.out.println(allQuerySubMatches2.toString());


            //    Set<String> starting2 = allQuerySubMatches1.keySet();
            //   System.out.println("We have "+ allQuerySubMatches1.keySet().size()+ " sucesful starting nodes identified using hash in first search");
            //  Set<String> starting1 = allQuerySubMatches2.keySet();
            Set<String> succesfulStartingnodes = new HashSet<String>();

            //  for (String nodes : starting2) {
            //     if (starting1.contains(nodes)) {
            //         succesfulStartingnodes.add(nodes);

            //    }

            //    }

            //  System.out.println("We have "+ succesfulStartingnodes.size()+ " sucesful starting nodes in the hashmaps");


            if (allMatches2.size() == 0) {
                System.out.println("No matches found");

            } else {
                sub_order_2 = splitrun2.getSubOrder();
                Set<String> vertices = originalSub.getSub().vertexSet();
                String[] temp = new String[vertices.size()];
                int size = 0;
                for (String node : vertices) {

                    temp[size] = node;
                    size++;

                }

                original_order = temp;

                mergeMatches();
                orderConverter();
                scoreSubs();
                // finalCheck();
            }
        }
        endTime = System.currentTimeMillis();

    }

    public void mergeMatches() {


        int count = 0;

        for (String match : allMatches1) {
            String[] match1 = match.split("\t");
            for (String match2 : allMatches2) {
                String[] match3 = match2.split("\t");
                if (match1[0].equals(match3[0])) {
                    count++;
                    String merged = match;
                    //don;t need the first element from the secind sub- it is the same in both
                    //it is the most connected sub
                    for (int p = 1; p < match3.length; p++) {
                        merged = merged + "\t" + match3[p];
                    }

                    mergedMatches.add(merged);

                }



            }


        }
        System.out.println("We have: " + count + " potential matches");

    }

    public void orderConverter() {

        String[] nodeOrder = new String[original_order.length];
        int[] orderConverter2 = new int[original_order.length];

        for (int k = 0; k < sub_order_1.length; k++) {
            nodeOrder[k] = sub_order_1[k];

        }
        System.out.println(Arrays.deepToString(nodeOrder));
        for (int k = 0; k < sub_order_2.length; k++) {
            if (k > 0) {
                nodeOrder[k + sub_order_1.length - 1] = sub_order_2[k];
            }

        }


        for (int h = 0; h < nodeOrder.length; h++) {
            for (int g = 0; g < original_order.length; g++) {
                if (nodeOrder[h].equals(original_order[g])) {
                    orderConverter2[h] = g;

                }
            }


        }

        orderConverter = orderConverter2;


    }

    public void finalCheck() {
        int wromg = 0;

        for (String math : scoredMatches) {
            String[] split = math.split("\t");
            for (String math2 : scoredMatches) {
                String[] split2 = math2.split("\t");
                if (split2[2].equals(split[3]) && split2[3].equals(split[2])) {
                    wromg++;
                }

                if (split2[4].equals(split[5]) && split2[5].equals(split[4])) {
                    wromg++;
                }
            }

        }

        System.out.println("wrong: " + wromg);

    }

    public void scoreSubs() throws Exception {

        //System.out.println(Arrays.deepToString(original_order));

        //Set<DefaultEdge> origEdges = originalSub.getSub().edgeSet();

        int failed = 0;

        for (String math : mergedMatches) {
            boolean check = true;
            String[] split = math.split("\t");
            String[] correctOrder = new String[split.length];


            //put in right order
            for (int y = 0; y < split.length; y++) {
                correctOrder[orderConverter[y]] = split[y];
            }

            //check for any repeated nodes
            for (int y = 0; y < correctOrder.length; y++) {
                String test = correctOrder[y];
                for (int u = 0; u < correctOrder.length; u++) {
                    if (u != y) {
                        if (correctOrder[u].equals(test)) {
                            check = false;

                        }
                    }

                }


            }

            //sdc check
            for (int j = 0; j < correctOrder.length; j++) {
                String QueryCC = origiSem.get(original_order[j]);
                String targetConceptClass = source.getConceptClass(correctOrder[j]);
                if (sdc.getNodeDistance(QueryCC, targetConceptClass) >= threshold) {
                } else {
                    check = false;
                }
            }

            //System.out.println(Arrays.deepToString(correctOrder));

            for (int h = 0; h < original_order.length; h++) {
                for (int g = 0; g < original_order.length; g++) {

                    if (originalSub.getSub().containsEdge(original_order[h], original_order[g]) == true && source.getSourceGraph().containsEdge(correctOrder[h], correctOrder[g]) == false) {
                        check = false;

                        break;
                    }

                    if (originalSub.getSub().containsEdge(original_order[g], original_order[h]) == true && source.getSourceGraph().containsEdge(correctOrder[g], correctOrder[h]) == false) {
                        check = false;

                        break;
                    }

                    //closed world checks
                    if ((originalSub.getSub().containsEdge(original_order[h], original_order[g]) == false) && source.getSourceGraph().containsEdge(correctOrder[h], correctOrder[g]) == true) {


                        check = false;

                        break;
                    }

                    if ((originalSub.getSub().containsEdge(original_order[g], original_order[h]) == false) && source.getSourceGraph().containsEdge(correctOrder[g], correctOrder[h]) == true) {

                        check = false;

                        break;
                    }



                }


            }

            if (check == true) {
                if (!scoredMatches.contains(math)) {
                    scoredMatches.add(math);
                }
            } else {
                failed++;
            }

        }

        System.out.println("We have: " + failed + " failing");
        System.out.println("We have: " + scoredMatches.size() + " scoredMatches");

    }

    public ArrayList<String> getScoredMatches() {

        return scoredMatches;
    }

    public QueryGraph getNextGraph(int order) {
        QueryGraph nextGraph = null;
        if (order == 1) {

            for (QueryGraph q : allQueries) {
                Map<String, String> seminfo = q.getQueryNodeInfo();
                Set<String> vertices = q.getSub().vertexSet();
                for (String vet : vertices) {
                    if (seminfo.get(vet).equals("Compound")) {
                        nextGraph = q;

                    }

                }
            }
        } else {
            if (remainingQueries.size() == 1) {

                for (QueryGraph w : remainingQueries) {
                    nextGraph = w;
                }
            }
        }

        System.out.println("Returning: " + nextGraph.getSub().toString());
        remainingQueries.remove(nextGraph);

        return nextGraph;
    }

    public QueryGraph getOrigiSub() {

        return originalSub;
    }

    public String getMappingTime() {
        String time = (" " + (double)(endTime - startTime) / 1000);
        return time.trim();
    }

    public double getSearchTime() {
        return searchTime;

    }
}
