package vfalgorithm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import vfalgorithm.QueryGraph.SemanticSub;

public class SemSearch extends SourceGraph {

    public static void main(String[] args) throws IOException, Exception {
        SerialisedGraph ser = new SerialisedGraph();
        SourceGraph source = ser.useSerialized("/Users/joemullen/Dropbox/VFAlgorithm/myobject.data");

        // RandomGraph rg = new RandomGraph();
        //rg.createSemanticGraph(source.getGraphNodeSize(), source.getAverageConnectivityOfEachConceptClass(), 100000, 1);

        SemanticDistanceCalculator sdc = new SemanticDistanceCalculator();

        QueryGraph q = new QueryGraph(SemanticSub.SIMCOMPSSMALL);
        // RandomQuery q = new RandomQuery(source.getAverageConnectivityOfEachConceptClass(), 3, "Compound");
        //  SourceGraph s3 = rg.getRandomSourceGraph();
        SemSearch sem11 = new SemSearch(source, q, 6, "Compound", true, 0.8, true, false);
        sem11.completeSearch();
        System.out.println("Suces init nodes: " + sem11.getSuccesfulInitialNodes().size());
        System.out.println(sem11.getSuccesfulInitialNodes().toString());
        sem11 = null;

        //graph pruning step

        GraphPruning gp4 = new GraphPruning(source, q, sdc, 0.8);
        SemSearch sem8 = new SemSearch(gp4.getPrunedSource(), q, 6, "Compound", true, 0.8, true, false);
        sem8.completeSearch();

        sem8 = null;
        gp4 = null;

//
    }

    public String getSummary() {
        return summary;
    }

    // constructor
    public SemSearch(SourceGraph s, QueryGraph q, int semDepth, String startClass, boolean SDC, double threshold, boolean closedWorld, boolean outPut) throws IOException {

        this.closedWorld = closedWorld;
        this.threshold = threshold;
        this.useSDC = SDC;
        this.writeToFile = outPut;
        this.semDepth = semDepth;
        this.source = s;
        this.query = q;
        this.motif = q.getSub();
        this.targ = s.getSourceGraph();
        this.subMap = q.nameEdgeset(motif);
        this.targeyMap = s.nameEdgeset(targ);
        this.subGraphSize = getGraphSize(motif);
        this.tarGraphSize = getGraphSize(targ);
        this.mostConnectedSemNode = mostConnectedSem(subMap, startClass, q.getQueryNodeInfo());
        this.mostConnectedTopNode = mostConnectedTop(subMap);

        if (semDepth == 0) {//just do it topologically
            this.initialNodes = s.startingNodes(mostConnectedTopNode,
                    subMap, targeyMap);
        } else {
            this.initialNodes = s.firstSemanticMatch(s.startingNodes(mostConnectedSemNode,
                    subMap, targeyMap), getStartingCC());
        }

        this.sub_core = new String[subGraphSize];
        this.sub_order = new String[subGraphSize];
        this.tar_core = new String[subGraphSize];
        this.sub_out = new String[subGraphSize];
        this.sub_in = new String[subGraphSize];
        this.tar_out = new String[tarGraphSize];
        this.tar_in = new String[tarGraphSize];
        this.potentialNodesPairsP = new String[tarGraphSize];
        this.allMatches2 = new ArrayList<String>();
        this.conceptInfo = s.getAllCOnceptInfo();
        this.sub_concept_classes = q.getQueryNodeInfo();

        if (useSDC == true) {
            this.sdc = new SemanticDistanceCalculator();
            this.allCCsINSDC = sdc.getAllConceptClasses();

        }

        this.succesfulInitialNodes = new HashSet<String>();

        //for now we need this- haven't included the new ccs and rts in the matrix yet



    }
    private Set<String> allCCsINSDC;
    private double threshold;
    private SemanticDistanceCalculator sdc;
    private boolean useSDC;
    private boolean writeToFile;
    public String summary = "";
    private int semDepth;
    private SourceGraph source;
    private QueryGraph query;
    int tempCount = 0;
    private String mostConnectedTopNode;
    private String mostConnectedSemNode;
    private DirectedGraph<String, DefaultEdge> targ;
    private DirectedGraph<String, DefaultEdge> motif;
    private HashMap<String, int[]> subMap;
    private HashMap<String, int[]> targeyMap;
    private int subGraphSize;
    private int tarGraphSize;
    private String[] sub_core;
    private String[] tar_core;
    private String[] UID_core;
    private String[] sub_order;
    private Map<String, String> sub_concept_classes;
    private String[] temp_new_additions;
    private int subOrderCount = 0;
    private int recursionCount = 0;
    private String[] sub_out;
    private String[] sub_in;
    private String[] tar_out;
    private String[] tar_in;
    private String[] nextResukst;
    private int UID = 0;
    private int STATE = 0;
    private int allMatchesExclSmallWorldAss = 0;
    private String[] potentialNodesPairsP;
    // private String[][] allMatches;
    private ArrayList<String> allMatches2;
    private ArrayList<String> initialNodes;
    private Set<String> succesfulInitialNodes;
    private DirectedGraph<String, DefaultEdge> subTree = new DefaultDirectedGraph<String, DefaultEdge>(
            DefaultEdge.class);
    private DirectedGraph<String, DefaultEdge> UIDTree = new DefaultDirectedGraph<String, DefaultEdge>(
            DefaultEdge.class);
    private String rootNodeSub = "";
    private String rootNodeUID = "";
    private String[][] conceptInfo;
    private boolean closedWorld;
    FileWriter fstream = new FileWriter("matchesWSubGSize_" + subGraphSize
            + ".txt");
    BufferedWriter out = new BufferedWriter(fstream);
    long startTime2;
    long endTime2;

    // for each initial pair we need to return an array list of strings- next
    // pairs
    // then we can call the method that takes the array list in a while loop,
    // taking the output of the previous
    public void completeSearch() throws IOException {

        startTime2 = System.currentTimeMillis();
        try {
            recursiveVF2(initialNodes);
        } catch (Exception ex) {
            Logger.getLogger(SemSearch.class.getName()).log(Level.SEVERE, null, ex);
        }
        endTime2 = System.currentTimeMillis();


        summary = "-----------------------------------------------------" + "\n" + "[SemSearch Took " + (endTime2 - startTime2) / 1000 + " seconds]" +"\n"+ "[Found: " + allMatches2.size() + " matches at SemDepth: " + semDepth + "]"+"\n"+ "[Matches Failing Closed World: " + (allMatchesExclSmallWorldAss - allMatches2.size()) + "]" +"[Initial Cand Size: " + initialNodes.size() + "]" + "[Subgraph size= " + subGraphSize +"/"+ query.getSub().edgeSet().size() + "]" +"[TarGraphSize= " + tarGraphSize +"/"+targ.edgeSet().size()+"]";

        System.out.println(summary);
        out.close();

    }

    public void recursiveVF2(ArrayList<String> initialPairs) throws Exception {

        prepareDataStructures();

        int numberCheckked = 1;

        for (String pairs : initialPairs) {
            long checktime = System.currentTimeMillis();
          
            recursionCount = 0;

            subTree = new DefaultDirectedGraph<String, DefaultEdge>(
                    DefaultEdge.class);
            UIDTree = new DefaultDirectedGraph<String, DefaultEdge>(
                    DefaultEdge.class);

            resetDataStructures();

            STATE = 0;

            String[] split = pairs.split(",,,");

            String sub = split[0];
            String tar = split[1];

            rootNodeSub = sub.trim();
            rootNodeUID = tar.trim();

            // searchTree.addVertex(rootNode);
            subTree.addVertex(rootNodeSub);
            UIDTree.addVertex(rootNodeUID);

//            System.out
//                    .println("-----------------------------------------------STARTING SEARCH WITH CANDIDATE PAIR: "
//                    + sub
//                    + ":"
//                    + tar
//                    + ".......   ("
//                    + numberCheckked
//                    + " of " + initialNodes.size() + ")");

            numberCheckked++;

            String[] resultsSTATE1 = getNextNodes(sub, tar);

            recursionCount++;

            nextResukst = getNextNodes(resultsSTATE1);

        }

    }

    public String[] getNextNodes(String subNode, String tarNode) throws Exception {

        resetDataStructures();

        String[][] info = popUID_Core(rootNodeUID, tarNode);

        sub_core = popSub_Core(rootNodeSub, subNode);

        UID_core = info[0];

        tar_core = info[1];

        STATE = getState();

        String allNodesAdded = "";

        populateArrays(sub_core, tar_core);

        calculateP();

        for (int x = 0; x < potentialNodesPairsP.length - 1; x++) {
            //for (int x = 0; x < 20; x++) {

            String pairs = potentialNodesPairsP[x];

            if (pairs.equals("NULL_NODE")) {

                break;
            } else {
                String[] split = pairs.split(",,,");
                String sub = split[0].trim();
                String tart = split[1].trim();
                String[] target = removeUID(tart);
                String tar = target[0].trim();


                int count = 0;

                // System.out.println("recursion Count:" + recursionCount);

                if ((feas_2(sub, tar) == true)
                        && (repeatabiltyCheck(tar, tar_core) == 0 && repeatabiltyCheck(
                        sub, sub_core) == 0)
                        && (feas_1(sub, tar) == true)
                        //&& (matchConceptClass(tar, sub) == true) 
                        && (checkSDC(tar, sub) == true) && (closedWorld(tar_core, closedWorld) == true)) {


                    allNodesAdded = allNodesAdded + pairs + "\t";
                    // String nodeUID = tar.trim() + "===" + (recursionCount +
                    // 1);
                    UIDTree.addVertex(tart);

                    // only need to add the sub node ONCE!!!
                    if (count == 0) {
                        subTree.addVertex(sub.trim());
                    }

                    STATE = getState();

                    UIDTree.addEdge(UID_core[STATE - 1].trim(), tart);

                    if (count == 0) {
                        subTree.addEdge(sub_core[STATE - 1].trim(), sub.trim());

                    }

                    if (STATE == subGraphSize - 1) {

                        String[] match1 = new String[subGraphSize];
                        String match = "";

                        for (int f = 0; f < match1.length; f++) {

                            if (f < subGraphSize - 1) {
                                match1[f] = tar_core[f];

                                match = match + tar_core[f] + "\t";
                                if (subOrderCount == 0) {
                                    sub_order[f] = sub_core[f];

                                }
                                // System.out.println(Arrays
                                //       .deepToString(sub_order));
                            } else {
                                match1[f] = tar;
                                match = match + tar;
                                if (subOrderCount == 0) {
                                    sub_order[f] = sub;

                                }
                            }

                        }

                        subOrderCount++;

                        if (closedWorld(match1, true) == true) {
                            if (allMatches2.contains(match)) {
                                // System.out.println("already added");
                                tempCount++;
                            } else {

                                allMatches2.add(match);
                                // System.out.println("match!"
                                // + (Arrays.deepToString(match1)));
                                allMatchesExclSmallWorldAss++;
                                //add the intial node to the set of succesful initial nodes
                                succesfulInitialNodes.add(match1[0]);


                                try {
                                    if (writeToFile == true) {
                                        addtoFile(match1);
                                    }
                                } catch (IOException ex) {
                                    Logger.getLogger(SemSearch.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }

                        }

                    } else {

                        // System.out.println("failed closed world assumption: ");
                        allMatchesExclSmallWorldAss++;

                    }
                    count++;

                } else {
                    // System.out.println("Searchtree exhausted");
                }
            }
        }

        String[] allNodesAddedSplit = allNodesAdded.split("\t");
        if (!allNodesAddedSplit[0].equals("")) {
        } else {
            String[] temp = new String[]{"Nothing"};
            allNodesAddedSplit = temp;
        }

        return allNodesAddedSplit;

    }

    public String[] getNextNodes(String[] otherNodes) throws Exception {

        String newAdditions = "";

        for (int u = 0; u < otherNodes.length; u++) {

            String local = otherNodes[u];

            String[] split = local.split(",,,");
            String sub = split[0].trim();
            if (sub.equals("Nothing")) {
                break;
            }
            String tar = split[1].trim();

            String[] local2 = getNextNodes(sub, tar);
            for (int t = 0; t < local2.length; t++) {
                newAdditions = newAdditions + local2[t].trim() + "\t";
            }
        }

        String[] newAdditions2 = newAdditions.split("\t");

        temp_new_additions = newAdditions2;

        recursionCount++;

        // System.out.println("these are added at recursion: "+recursionCount+" "+
        // Arrays.deepToString(temp_new_additions));
        //System.out.println("done check for recursioncount: " + recursionCount);
        //System.out
        //	.println("new_additions_length: " + temp_new_additions.length);
        while (recursionCount < subGraphSize - 1
                && temp_new_additions.length > 1
                && !temp_new_additions[0].equals("Nothing")) {
            getNextNodes(temp_new_additions);
        }

        return newAdditions2;

    }

    public String addUID(String node) {
        String unique = node + "===" + UID;
        UID++;
        return unique;
    }

    public String[] removeUID(String node) {
        String[] split1 = node.split("===");
        String[] split = new String[2];
        for (int p = 0; p < split1.length; p++) {
            split[p] = split1[p].trim();
        }
        return split;

    }

    public String[][] popUID_Core(String nodeInQuestion, String targetNode) {
        // System.out.println(nodeInQuestion + "  " + targetNode + "  "
        // + UIDTree.toString());
        DijkstraShortestPath tr2 = new DijkstraShortestPath(UIDTree,
                nodeInQuestion, targetNode);

        Object[] why = tr2.getPathEdgeList().toArray();
        String[][] output = new String[2][subGraphSize];
        String[] tempTar = new String[subGraphSize];
        if (why.length > 0) {
            String nodes = "";
            for (int g = 0; g < why.length; g++) {

                String relation1 = why[g].toString();
                // System.out.println("pop UID-COre:" + relation1);
                String relation2 = relation1.trim();
                String relation = relation2
                        .substring(1, relation2.length() - 1);

                String[] split = relation.split(":");
                // System.out
                // .println("pop UID-COre:" + Arrays.deepToString(split));
                for (int y = 0; y < split.length; y++) {
                    String node = split[y].trim();
                    if (g == 0) {
                        nodes = nodes + node + "\t";
                        // System.out.println("g (should be 0):" + g);
                    } else if (y == 0) {
                        nodes = nodes + split[1].trim() + "\t";
                        // System.out.println("g (should be >0: " + g);
                    }
                }

            }
            // System.out.println("NODES: " + nodes);

            String[] core = nodes.split("\t");
            for (int h = 0; h < core.length; h++) {
                if (!core[h].equals("")) {
                    tempTar[h] = core[h];
                }
            }
            for (int f = 0; f < tempTar.length; f++) {
                if (tempTar[f] == null) {
                    tempTar[f] = "NULL_NODE";
                }
            }
        } else {
            for (int f = 0; f < tempTar.length; f++) {
                if (f == 0) {
                    tempTar[f] = nodeInQuestion;
                } else {
                    tempTar[f] = "NULL_NODE";
                }

            }
        }

        output[0] = tempTar;
        for (int y = 0; y < tempTar.length; y++) {
            if (output[0][y].equals("NULL_NODE")) {
                output[1][y] = output[0][y];
            } else {
                String[] local = removeUID(output[0][y]);
                output[1][y] = local[0];

            }
        }
        return output;
    }

    public String[] popSub_Core(String nodeInQuestion, String targetNode) {

        DijkstraShortestPath tr2 = new DijkstraShortestPath(subTree,
                nodeInQuestion, targetNode);

        Object[] why = tr2.getPathEdgeList().toArray();

        String[] tempTar = new String[subGraphSize];
        if (why.length > 0) {
            String nodes = "";
            for (int g = 0; g < why.length; g++) {

                String relation1 = why[g].toString();
                // System.out.println("pop Sub-COre:" + relation1);
                String relation2 = relation1.trim();
                String relation = relation2
                        .substring(1, relation2.length() - 1);

                String[] split = relation.split(":");
                // System.out
                // .println("pop Sub-COre:" + Arrays.deepToString(split));
                for (int y = 0; y < split.length; y++) {
                    String node = split[y].trim();
                    if (g == 0) {
                        nodes = nodes + node + "\t";
                        // System.out.println("g (should be 0):" + g);
                    } else if (y == 0) {
                        nodes = nodes + split[1].trim() + "\t";
                        // System.out.println("g (should be >0: " + g);
                    }
                }

            }
            // System.out.println("NODES: " + nodes);

            String[] core = nodes.split("\t");
            for (int h = 0; h < core.length; h++) {
                if (!core[h].equals("")) {
                    tempTar[h] = core[h];
                }
            }
            for (int f = 0; f < tempTar.length; f++) {
                if (tempTar[f] == null) {
                    tempTar[f] = "NULL_NODE";
                }
            }
        } else {
            for (int f = 0; f < tempTar.length; f++) {
                if (f == 0) {
                    tempTar[f] = nodeInQuestion;
                } else {
                    tempTar[f] = "NULL_NODE";
                }

            }
        }
        // need to fill the rest with "null_nodes"?!?!?!
        // System.out.println("Sub_core fter pop:" +
        // Arrays.deepToString(tempTar));
        return tempTar;
    }

    public boolean feas_1(String subNode, String tarNode) {

        boolean well = false;

        if (STATE == 0) {

            well = true;
        } else {
            if ((R_pred(subNode, tarNode) == true)
                    && (R_succ(subNode, tarNode) == true)) {

                well = true;

            } else {
                // System.out.println("failed feas_1");
            }
        }

        return well;

    }

    public boolean R_pred(String sub, String tar) {
        // predecessors (COMING IN)
        // we want to know if the pairs match in the same way to the previous
        // nodes of the partial mapping
        boolean well = true;
        String[] inComingNodes = tIn(motif, sub);

        if (inComingNodes.length > 0) {

            // get all outgoing nodes if the sub node of the pair

            // System.out.println("R_pred 1 of sub : "
            // + Arrays.deepToString(inComingNodes));
            // finds the location within sub_core if the nodes have already been
            // added to the partial mapping

            ArrayList<Integer> location_in_mapping = new ArrayList<Integer>();

            for (String node : inComingNodes) {
                for (int i = 0; i < sub_core.length - 1; i++) {
                    if (sub_core[i].equals(node)) {
                        location_in_mapping.add(i);// up to here!!
                    } else {
                        // we have none to look at
                        well = true;
                        // System.out.println("R_pred 2: nothing to look at");
                    }
                }
            }

            for (int look : location_in_mapping) {
                // System.out.println("looking for an edge between:  "
                // + tar_core[look] + "  " + tar);
                if (targ.containsEdge(tar_core[look].trim(), tar.trim()) == true) {
                    well = true;
                    // System.out.println("passed R_pred");
                } else {
                    well = false;
                    // System.out.println("failed R_pred");
                }
            }
        }

        return well;

    }

    public boolean R_succ(String sub, String tar) {
        // successors (GOING OUT)
        boolean well = true;
        // get all outgoing nodes if the sub node of the pair
        String[] outGoingNodes = tOut(motif, sub);

        if (outGoingNodes.length > 0) {
            // finds the location within sub_core if the nodes have already been
            // added to the partial mapping

            ArrayList<Integer> location_in_mapping = new ArrayList<Integer>();

            for (String node : outGoingNodes) {
                for (int i = 0; i < sub_core.length - 1; i++) {
                    if (sub_core[i].equals(node)) {
                        location_in_mapping.add(i);// up to here!!
                    } else {
                        // we have none to look at
                        well = true;
                        // System.out.println("R_succ 2: nothing to look at");
                    }
                }
            }
            // System.out.println("locations of edges: " + location_in_mapping);
            for (int look : location_in_mapping) {
                if (targ.containsEdge(tar.trim(), tar_core[look].trim()) == true) {
                    well = true;
                    // System.out.println("passed R_succ");

                } else {
                    // System.out.println("no edge between tr:" + tar
                    // + "tar look: " + tar_core[look]);
                    well = false;

                    // System.out.println("failed R_succ");

                }

            }
        }

        return well;
    }

    public boolean feas_2(String subNode, String tarNode) {

        // System.out.println("feas_2: sub= " + subNode + "  tar= " + tarNode);

        if (STATE == 0) {
            return true;

        } else {
            int[] subEdgeset = subMap.get(subNode);
            int[] tarEdgeset = targeyMap.get(tarNode);
            return (R_termin(subEdgeset, tarEdgeset));
        }

    }

    public boolean R_termin(int[] sub, int[] tar) {

        boolean well = true;

        for (int p = 0; p < sub.length; p++) {

            if (tar[p] < sub[p]) {
                well = false;
                // System.out.println("failed feas_2");
            }
        }
        return well;
    }

    public boolean R_termout(String sub) {

        return false;

    }

    public void feas_3() {
        // will call R_new
    }

    public boolean R_new() {

        return false;

    }

    public void resetDataStructures() {
        for (int i = 0; i < subGraphSize; i++) {
            if (sub_core[i].equals("NULL_NODE")) {
                break;

            } else {
                sub_core[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < subGraphSize; i++) {
            if (tar_core[i].equals("NULL_NODE")) {
                break;

            } else {

                tar_core[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < subGraphSize; i++) {
            if (sub_out[i].equals("NULL_NODE")) {
                break;

            } else {
                sub_out[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < subGraphSize; i++) {
            if (sub_in[i].equals("NULL_NODE")) {
                break;

            } else {
                sub_in[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < tarGraphSize; i++) {
            if (tar_out[i].equals("NULL_NODE")) {
                break;

            } else {
                tar_out[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < tarGraphSize; i++) {
            if (tar_in[i].equals("NULL_NODE")) {
                break;

            } else {
                tar_in[i] = "NULL_NODE";
            }
        }
        for (int i = 0; i < tarGraphSize; i++) {
            if (potentialNodesPairsP[i].equals("NULL_NODE")) {
                break;

            } else {
                potentialNodesPairsP[i] = "NULL_NODE";
            }
        }
    }

    public void prepareDataStructures() {
        for (int i = 0; i < subGraphSize; i++) {
            sub_core[i] = "NULL_NODE";
            tar_core[i] = "NULL_NODE";
            sub_out[i] = "NULL_NODE";
            sub_in[i] = "NULL_NODE";
        }

        for (int i = 0; i < tarGraphSize; i++) {
            tar_out[i] = "NULL_NODE";
            tar_in[i] = "NULL_NODE";
            potentialNodesPairsP[i] = "NULL_NODE";

        }

    }

    public void populateArrays(String[] subnode, String[] tarnode) {

        String[] tar_out1;
        tar_out1 = tOut(targ, tarnode);

        for (int i = 0; i < tar_out1.length; i++) {

            tar_out[i] = addUID(tar_out1[i]);
        }
        String[] tar_in1;
        tar_in1 = tIn(targ, tarnode);

        for (int i = 0; i < tar_in1.length; i++) {
            tar_in[i] = addUID(tar_in1[i]);
        }

        String[] sub_out1;
        sub_out1 = tOut(motif, subnode);
        for (int i = 0; i < sub_out1.length; i++) {
            sub_out[i] = sub_out1[i];
        }

        String[] sub_in1;
        sub_in1 = tIn(motif, subnode);
        for (int i = 0; i < sub_in1.length; i++) {
            sub_in[i] = sub_in1[i];
        }
    }

    public String calculateP() {
        String mostConnectednode = "";
        int highest = 0;

        if (!tar_out[0].equals("NULL_NODE") && !sub_out[0].equals("NULL_NODE")) {

            for (int y = 0; y < sub_out.length - 1; y++) {
                String local = sub_out[y];
                if (!sub_out[y].equals("NULL_NODE")) {
                    int[] local2 = subMap.get(local);

                    if ((local2[0] >= highest)
                            && (repeatabiltyCheck(local, sub_core) == 0)) {
                        highest = local2[0];
                        mostConnectednode = local;

                    }

                }
            }

            for (int i = 0; i < tar_out.length; i++) {
                if (tar_out[i].equals("NULL_NODE")) {

                    break;
                } else if (!mostConnectednode.equals("")) {

                    potentialNodesPairsP[i] = mostConnectednode + ",,,"
                            + tar_out[i];

                }

            }
        }

        if (mostConnectednode.equals("") && !tar_in[0].equals("NULL_NODE")
                && !sub_in[0].equals("NULL_NODE")) {

            String mostConnectednodeIn = "";
            int highestIn = 0;

            for (int y = 0; y < sub_in.length - 1; y++) {

                String local = sub_in[y];
                if (!sub_in[y].equals("NULL_NODE")) {

                    int[] local2 = subMap.get(local);

                    if ((local2[0] >= highestIn)
                            && (repeatabiltyCheck(local, sub_core) == 0)) {
                        highestIn = local2[0];
                        mostConnectednodeIn = local;

                    }
                }
            }

            for (int i = 0; i < tar_in.length; i++) {

                if (tar_in[i].equals("NULL_NODE")) {

                    break;
                } else {

                    potentialNodesPairsP[i] = mostConnectednodeIn + ",,,"
                            + tar_in[i];

                }
            }

        } else {
            // System.out.println("no more nodes to explore");
            // this is where we would look at all the remaining node
        }
        return mostConnectednode;

    }

    public boolean completeMatch() {
        boolean completeMatch = false;

        if (getGraphSize(targ) == getGraphSize(motif)) {
            completeMatch = true;

        }
        return completeMatch;

    }

    public int getState() {

        int county = 0;
        for (int y = 0; y < sub_core.length; y++) {
            if (sub_core[y] == "NULL_NODE") {
                county++;
            }

        }
        int myPredictedState = (sub_core.length - county);

        return myPredictedState;

    }

    public String[] getMatchRelationsRELTYPE(String[] match) {

        String[] local = match;
        String[] reltypes = new String[match.length];

        for (int x = 0; x < local.length; x++) {
            String[] split = local[x].split(":");
            String first = split[0].trim();
            String second = split[1].trim();
            String relationType = source.getRelationType(first, second);

            reltypes[x] = relationType;
        }

        return reltypes;



    }

    public String[] getMatchRelations(String[] match) {

        String subEdge = motif.edgeSet().toString();
        String[] lomatch = match;

        for (int u = 0; u < sub_order.length; u++) {
            subEdge = subEdge.replaceAll(sub_order[u], lomatch[u]);

        }
        String[] subedge2 = subEdge.substring(1,
                subEdge.length() - 1).split(", ");

        for (int h = 0; h < subedge2.length; h++) {
            subedge2[h] = subedge2[h].trim().substring(1,
                    subedge2[h].length() - 1);

        }


        return subedge2;
    }

    public int repeatabiltyCheck(String node, String[] contained) {
        int ma = 0;
        if (STATE > 0) {

            for (int i = 0; i <= contained.length - 1; i++) {
                String here = contained[i];
                if (node.equals(here)) {
                    ma++;
                }
            }

        } else {

            ma = 0;
        }
        // System.out.println("repeatabilty check: " + node + " score: " + ma);
        return ma;
    }

    public void addtoFile(String[] match1)
            throws IOException {

        String[] rel = getMatchRelations(match1);
        String[] relType = getMatchRelationsRELTYPE(rel);

        String[] conCl = getConceptClass(match1);
        String[] conName = getConceptName(match1);


        try {
            out.append(">>> " + (allMatches2.size()) + "\n");

            String[] localNodes = match1;
            out.append("SN:");
            for (int t = 0; t < localNodes.length; t++) {
                out.append(localNodes[t] + "\t");
            }
            out.append("\n");

            String[] localConcepts = conName;
            out.append("NN:");
            for (int t = 0; t < localConcepts.length; t++) {
                out.append(localConcepts[t] + "\t");
            }
            out.append("\n");

            String[] localClasses = conCl;
            out.append("CC:");
            for (int t = 0; t < localClasses.length; t++) {
                out.append(localClasses[t] + "\t");
            }
            out.append("\n");

            String[] localReltype = rel;
            out.append("SR:");
            for (int t = 0; t < localReltype.length; t++) {
                out.append(localReltype[t] + "\t");
            }
            out.append("\n");

            String[] localRel = relType;
            out.append("RT:");
            for (int t = 0; t < localRel.length; t++) {
                out.append(localRel[t] + "\t");
            }
            out.append("\n");

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());

        }



    }

    public boolean closedWorld(String[] match, boolean run) {

        boolean test = true;
        //if we aren't using a closed world approach just return true
        if (run == false) {
            return true;

        } else {

            for (int u = 0; u < sub_order.length; u++) {
                if (sub_order[u] == null) {
                    break;
                }
                for (int i = 0; i < sub_order.length; i++) {
                    if (sub_order[i] == null) {
                        break;
                    }
                    if ((motif.containsEdge(sub_order[u], sub_order[i]) == false) && targ
                            .containsEdge(match[u], match[i]) == true) {

                        return false;
                    }

                    if ((motif.containsEdge(sub_order[i], sub_order[u]) == false) && (targ
                            .containsEdge(match[i], match[u]) == true)) {
                        return false;
                    }

                }

            }
        }
        return test;
    }

    public String getConceptName(String nodeName) {
        int local = Integer.parseInt(nodeName);
        int local2 = local - 1;
        return conceptInfo[local2][3].trim();
    }

    public String[] getConceptName(String[] local) {
        String[] ConIDs = new String[local.length];
        for (int i = 0; i < ConIDs.length; i++) {
            ConIDs[i] = getConceptName(local[i]);
        }
        return ConIDs;
    }

    public String getConceptClass(String nodeName) {
        int local = Integer.parseInt(nodeName);
        int local2 = local - 1;
        return conceptInfo[local2][2];
    }

    public String[] getConceptClass(String[] local) {
        String[] ConIDs = new String[local.length];
        for (int i = 0; i < ConIDs.length; i++) {
            ConIDs[i] = getConceptClass(local[i]);
        }
        return ConIDs;
    }

    public boolean matchConceptClass(String tarNode, String subNode) {

        //boolean match = false;

        //we only want to do this check of we are not scoring using the SDC

        String QueryCC = sub_concept_classes.get(subNode);
        String targetConceptClass = getConceptClass(tarNode);

        if (useSDC == false) {

            if (targetConceptClass.equals("Compound") || targetConceptClass.equals("Comp")) {
                targetConceptClass = "Compound";
            }

            if (targetConceptClass.equals(" ")) {

                return true;

            } else if (targetConceptClass.equals(QueryCC) || STATE > semDepth) {

                return true;
            }
        } else {

            return true;
        }

        return false;

    }

    private boolean checkSDC(String tarNode, String subNode) throws Exception {



        if (useSDC == false) {

            return true;
        } else {
            String QueryCC = sub_concept_classes.get(subNode);
            String targetConceptClass = getConceptClass(tarNode);
            //System.out.println("CHECKING Sdc doesn't contain: " + QueryCC + "  " + targetConceptClass);
            //this if statement is there until we update the SDC!!
            //all CCs not in the SDC will return a false
            if (allCCsINSDC.contains(QueryCC) && (allCCsINSDC.contains(targetConceptClass))) {
                if (sdc.getNodeDistance(QueryCC, targetConceptClass) >= threshold) {

                    return true;
                }

            } else {


                return false;
            }


        }

        return false;

    }

    public ArrayList<String> getSuccesfulInitialNodes() {

        ArrayList<String> in = new ArrayList<String>();
        for (String init : initialNodes) {
            in.add(init);
        }
        return in;
    }

    public String getTime() {
        String time = (" " + (double) (endTime2 - startTime2) / 1000);
        return time.trim();
    }

    public ArrayList<String> getAllMatches() {
        return allMatches2;

    }
}
