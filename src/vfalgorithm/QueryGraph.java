package vfalgorithm;

import java.util.HashMap;
import java.util.Map;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author joemullen
 */
public class QueryGraph extends GraphMethods {

    SemanticSub semSubType;
    Map<String, String> queryNODEInfo = new HashMap<String, String>();
    Map<String, String> queryRELInfo = new HashMap<String, String>();
    String MOTIFNAME = "";
    DirectedGraph<String, DefaultEdge> Sub;

    public enum SemanticSub {

        SIMCOMPSSMALL, SIMTARGSSMALL, NEWTARGIND, SIMCOMSIMTAR, NEWINDICATION, IDENTIFYMOA, FIVENODE, SIXNODE, EIGHTNODE
    }

    public QueryGraph(DirectedGraph<String, DefaultEdge> graph, String motifname, Map<String, String> queryNODEInfo) {
        this.MOTIFNAME = motifname;
        this.Sub = graph;
        this.queryNODEInfo = queryNODEInfo;

    }

    public QueryGraph(SemanticSub sub) {
        this.semSubType = sub;

        switch (semSubType) {
            case SIMCOMPSSMALL:
                chlorpromMotif();
                break;

            case SIMTARGSSMALL:
                newTargetMotif();
                break;

            case NEWTARGIND:
                newTargetSMALL();
                break;

            case SIMCOMSIMTAR:
                newBothMotif();
                break;

            case NEWINDICATION:
                newIndications();
                break;

            case IDENTIFYMOA:
                identifyMoA();
                break;

            case FIVENODE:
                fiveNodeMotif();
                break;

            case SIXNODE:
                largeMotif();
                break;

            case EIGHTNODE:
                testForSplitEight();
                break;

        }

    }

    //produces a sub-graph for the chlorpromazine motif
    //linear
    private DirectedGraph<String, DefaultEdge> chlorpromMotif() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG Chlo");
        queryNODEInfo.put("DRUG Chlo", "Compound");
        subgraph.addVertex("DRUG Trim");
        queryNODEInfo.put("DRUG Trim", "Compound");
        subgraph.addVertex("TARGET");
        queryNODEInfo.put("TARGET", "Target");


        subgraph.addEdge(("DRUG Chlo"), ("DRUG Trim"));
        queryRELInfo.put("DRUG Chlo:DRUG Trim", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("DRUG Chlo"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("TARGET"));
        queryRELInfo.put("DRUG Trim:TARGET", "binds_to");

        MOTIFNAME = "CHLOR";

        //System.out.println("[INFO] Chlorprom sub created:  " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    private DirectedGraph<String, DefaultEdge> newTargetSMALL() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG Chlo");
        queryNODEInfo.put("DRUG Chlo", "Compound");
        subgraph.addVertex("TARGET");
        queryNODEInfo.put("TARGET", "Target");
        subgraph.addVertex("INDI");
        queryNODEInfo.put("INDI", "Indications");

        subgraph.addEdge(("DRUG Chlo"), ("INDI"));
        queryRELInfo.put("DRUG Chlo:INDI", "treats");
        subgraph.addEdge(("TARGET"), ("INDI"));
        queryRELInfo.put("TARGET:INDI", "binds_to");

        MOTIFNAME = "NEWTargetSMALL";

        //System.out.println("[INFO] New_target_SMALL sub created:  " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    private DirectedGraph<String, DefaultEdge> testForSplitEight() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG Chlo");
        queryNODEInfo.put("DRUG Chlo", "Compound");
        subgraph.addVertex("DRUG Trim");
        queryNODEInfo.put("DRUG Trim", "Compound");
        subgraph.addVertex("TARGET");
        queryNODEInfo.put("TARGET", "Target");
        subgraph.addVertex("INDI");
        queryNODEInfo.put("INDI", "Indications");
        subgraph.addVertex("Target2");
        queryNODEInfo.put("Target2", "Target");
        subgraph.addVertex("Indication2");
        queryNODEInfo.put("Indication2", "Indications");
        subgraph.addVertex("Protein1");
        queryNODEInfo.put("Protein1", "Protein");
        subgraph.addVertex("Protein2");
        queryNODEInfo.put("Protein2", "Protein");


        subgraph.addEdge(("DRUG Chlo"), ("DRUG Trim"));
        queryRELInfo.put("DRUG Chlo:DRUG Trim", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("DRUG Chlo"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("TARGET"));
        queryRELInfo.put("DRUG Trim:TARGET", "binds_to");
        subgraph.addEdge(("DRUG Chlo"), ("TARGET"));
        queryRELInfo.put("DRUG Chlo:TARGET", "binds_to");
        subgraph.addEdge(("TARGET"), ("INDI"));
        queryRELInfo.put("TARGET:INDI", "binds_to");
        subgraph.addEdge(("DRUG Chlo"), ("INDI"));
        queryRELInfo.put("DRUG Chlo:INDI", "binds_to");
        subgraph.addEdge(("TARGET"), ("Target2"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Target2"), ("TARGET"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Target2"), ("Protein1"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Protein1"), ("Protein2"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("Protein2"), ("Protein1"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("INDI"), ("Indication2"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");


        Sub = subgraph;
        MOTIFNAME = "NEWINLARGE";

        System.out.println("[INFO] New_Indication sub created:  " + "\n" + subgraph.toString());
        System.out.println("[INFO] ----------------------------------------------------------");

        return subgraph;

    }

    private DirectedGraph<String, DefaultEdge> newIndications() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG Chlo");
        queryNODEInfo.put("DRUG Chlo", "Compound");
        subgraph.addVertex("DRUG Trim");
        queryNODEInfo.put("DRUG Trim", "Compound");
        subgraph.addVertex("TARGET");
        queryNODEInfo.put("TARGET", "Target");
        subgraph.addVertex("INDI");
        queryNODEInfo.put("INDI", "Indications");


        subgraph.addEdge(("DRUG Chlo"), ("DRUG Trim"));
        queryRELInfo.put("DRUG Chlo:DRUG Trim", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("DRUG Chlo"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("TARGET"));
        queryRELInfo.put("DRUG Trim:TARGET", "binds_to");
        subgraph.addEdge(("DRUG Chlo"), ("TARGET"));
        queryRELInfo.put("DRUG Chlo:TARGET", "binds_to");
        subgraph.addEdge(("TARGET"), ("INDI"));
        queryRELInfo.put("TARGET:INDI", "binds_to");
        subgraph.addEdge(("DRUG Chlo"), ("INDI"));
        queryRELInfo.put("DRUG Chlo:INDI", "binds_to");

        Sub = subgraph;
        MOTIFNAME = "NEWINLARGE";

        System.out.println("[INFO] New_Indication sub created:  " + "\n" + subgraph.toString());
        System.out.println("[INFO] ----------------------------------------------------------");

        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> identifyMoA() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG Chlo");
        queryNODEInfo.put("DRUG Chlo", "Compound");
        subgraph.addVertex("DRUG Trim");
        queryNODEInfo.put("DRUG Trim", "Compound");
        subgraph.addVertex("TARGET");
        queryNODEInfo.put("TARGET", "Target");
        subgraph.addVertex("INDI");
        queryNODEInfo.put("INDI", "Indications");


        subgraph.addEdge(("DRUG Chlo"), ("DRUG Trim"));
        queryRELInfo.put("DRUG Chlo:DRUG Trim", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("DRUG Chlo"));
        queryRELInfo.put("DRUG Trim:DRUG Chlo", "similar_to");
        subgraph.addEdge(("DRUG Trim"), ("TARGET"));
        queryRELInfo.put("DRUG Trim:TARGET", "binds_to");
        subgraph.addEdge(("TARGET"), ("INDI"));
        queryRELInfo.put("TARGET:INDI", "binds_to");
        subgraph.addEdge(("DRUG Chlo"), ("INDI"));
        queryRELInfo.put("DRUG Chlo:INDI", "binds_to");
        subgraph.addEdge(("DRUG Trim"), ("INDI"));
        queryRELInfo.put("DRUG Trim:INDI", "binds_to");

        MOTIFNAME = "MOA";

        //System.out.println("[INFO] New_Indication sub created:  " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public Map<String, String> getQueryNodeInfo() {

        return queryNODEInfo;

    }

    public DirectedGraph<String, DefaultEdge> testMotif() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG Chlo");
        subgraph.addVertex("DRUG Trim");

        subgraph.addEdge(("DRUG Chlo"), ("DRUG Trim"));



        //System.out.println("[INFO] Test Sub Created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> fiveNodeMotif() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG1");
        queryNODEInfo.put("DRUG1", "Compound");
        subgraph.addVertex("DRUG2");
        queryNODEInfo.put("DRUG2", "Compound");
        subgraph.addVertex("TARGET1");
        queryNODEInfo.put("TARGET1", "Target");
        subgraph.addVertex("TARGET2");
        queryNODEInfo.put("TARGET2", "Target");
        subgraph.addVertex("INDI");
        queryNODEInfo.put("INDI", "Protein");


        subgraph.addEdge(("DRUG1"), ("TARGET1"));
        subgraph.addEdge(("TARGET1"), ("TARGET2"));
        subgraph.addEdge(("TARGET2"), ("TARGET1"));
        subgraph.addEdge(("DRUG1"), ("DRUG2"));
        subgraph.addEdge(("DRUG2"), ("DRUG1"));
        subgraph.addEdge(("TARGET2"), ("INDI"));


        MOTIFNAME = "five";

        //System.out.println("[INFO] Test Sub Created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> fiveNodeMotifPErf() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("1");
        subgraph.addVertex("2");
        subgraph.addVertex("3");
        subgraph.addVertex("4");
        subgraph.addVertex("5");

        subgraph.addEdge(("1"), ("2"));
        subgraph.addEdge(("3"), ("1"));
        subgraph.addEdge(("1"), ("3"));
        subgraph.addEdge(("4"), ("2"));
        subgraph.addEdge(("2"), ("4"));
        subgraph.addEdge(("4"), ("5"));

        MOTIFNAME = "five2";



        //System.out.println("[INFO] Test Sub Created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> newTargetMotif() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG");
        subgraph.addVertex("TARGET_1");
        subgraph.addVertex("TARGET_2");


        subgraph.addEdge(("DRUG"), ("TARGET_1"));
        subgraph.addEdge(("TARGET_1"), ("TARGET_2"));
        subgraph.addEdge(("TARGET_2"), ("TARGET_1"));

        System.out.println("[INFO] Finding new target sub created:  " + "\n" + subgraph.toString());
        System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;

        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> newBothMotif() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG_1");
        subgraph.addVertex("DRUG_2");
        subgraph.addVertex("TARGET_1");
        subgraph.addVertex("TARGET_2");


        subgraph.addEdge(("DRUG_1"), ("TARGET_1"));
        subgraph.addEdge(("TARGET_1"), ("TARGET_2"));
        subgraph.addEdge(("TARGET_2"), ("TARGET_1"));
        subgraph.addEdge(("DRUG_1"), ("DRUG_2"));
        subgraph.addEdge(("DRUG_2"), ("DRUG_1"));

        //System.out.println("[INFO] Finding new target sub created:  " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        MOTIFNAME = "new both";
        Sub = subgraph;

        return subgraph;

    }

    //produces a sub-graph for a modified version of the chlorpromazine motif
    //circular (i.e. the target has an edge to (Chlo)
    public DirectedGraph<String, DefaultEdge> circularMotif() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG Chlo");
        subgraph.addVertex("DRUG Trim");
        subgraph.addVertex("TARGET");


        subgraph.addEdge(("DRUG Chlo"), ("DRUG Trim"));
        subgraph.addEdge(("DRUG Trim"), ("DRUG Chlo"));
        subgraph.addEdge(("DRUG Trim"), ("TARGET"));
        subgraph.addEdge(("DRUG Chlo"), ("TARGET"));


        System.out.println("[INFO] Circular sub created: " + "\n" + subgraph.toString());
        System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> largeMotif() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG");
        queryNODEInfo.put("DRUG", "Compound");
        subgraph.addVertex("TARGET");
        queryNODEInfo.put("TARGET", "Target");
        subgraph.addVertex("PROTEIN_1");
        queryNODEInfo.put("PROTEIN_1", "Protein");
        subgraph.addVertex("PROTEIN_2");
        queryNODEInfo.put("PROTEIN_2", "Protein");
        subgraph.addVertex("DISEASE_1");
        queryNODEInfo.put("DISEASE_1", "Disease");
        subgraph.addVertex("DISEASE_2");
        queryNODEInfo.put("DISEASE_2", "Disease");


        subgraph.addEdge(("DRUG"), ("TARGET"));
        subgraph.addEdge(("TARGET"), ("PROTEIN_1"));
        subgraph.addEdge(("PROTEIN_1"), ("PROTEIN_2"));
        subgraph.addEdge(("PROTEIN_2"), ("PROTEIN_1"));
        subgraph.addEdge(("PROTEIN_1"), ("DISEASE_1"));
        subgraph.addEdge(("PROTEIN_2"), ("DISEASE_2"));
        MOTIFNAME = "large";


        //System.out.println("[INFO] Largesub created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> testThree() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG");
        subgraph.addVertex("TARGET");
        subgraph.addVertex("PROTEIN_1");

        subgraph.addEdge(("DRUG"), ("TARGET"));
        subgraph.addEdge(("TARGET"), ("PROTEIN_1"));

        MOTIFNAME = "test3";
        //System.out.println("[INFO] Largesub created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> testFour() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG");
        subgraph.addVertex("TARGET");
        subgraph.addVertex("PROTEIN_1");
        subgraph.addVertex("colin");

        subgraph.addEdge(("DRUG"), ("TARGET"));
        subgraph.addEdge(("TARGET"), ("PROTEIN_1"));
        subgraph.addEdge(("PROTEIN_1"), ("colin"));

        MOTIFNAME = "test4";
        //System.out.println("[INFO] Largesub created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> testFive() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG");
        subgraph.addVertex("TARGET");
        subgraph.addVertex("PROTEIN_1");
        subgraph.addVertex("colin");
        subgraph.addVertex("c");

        subgraph.addEdge(("DRUG"), ("TARGET"));
        subgraph.addEdge(("TARGET"), ("PROTEIN_1"));
        subgraph.addEdge(("PROTEIN_1"), ("colin"));
        subgraph.addEdge(("colin"), ("c"));

        MOTIFNAME = "test5";
        //System.out.println("[INFO] Largesub created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public DirectedGraph<String, DefaultEdge> testSix() {
        DirectedGraph<String, DefaultEdge> subgraph = new DefaultDirectedGraph<String, DefaultEdge>(
                DefaultEdge.class);

        subgraph.addVertex("DRUG");
        subgraph.addVertex("TARGET");
        subgraph.addVertex("PROTEIN_1");
        subgraph.addVertex("colin");
        subgraph.addVertex("c");
        subgraph.addVertex("d");

        subgraph.addEdge(("DRUG"), ("TARGET"));
        subgraph.addEdge(("TARGET"), ("PROTEIN_1"));
        subgraph.addEdge(("PROTEIN_1"), ("colin"));
        subgraph.addEdge(("colin"), ("c"));
        subgraph.addEdge(("c"), ("d"));

        MOTIFNAME = "test6";
        //System.out.println("[INFO] Largesub created: " + "\n" + subgraph.toString());
        //System.out.println("[INFO] ----------------------------------------------------------");
        Sub = subgraph;
        return subgraph;

    }

    public String getQueryNodeCC(String node) {
        if (queryNODEInfo.containsKey(node)) {
            return queryNODEInfo.get(node);

        } else {
            return "NOT_AVAILABLE";
        }


    }

    public String getQueryRelRT(String relation) {
        if (queryRELInfo.containsKey(relation)) {
            return queryNODEInfo.get(relation);

        } else {

            return "NOT AVAILABLE";
        }


    }

    public String getMotifName() {

        return MOTIFNAME;
    }

    public DirectedGraph<String, DefaultEdge> getSub() {
        return Sub;

    }
}
